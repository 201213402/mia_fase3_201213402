﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//-----------Estructuras--------------

typedef struct _datosDisco {
    char nombre[32];
    int tamanio;
    int cantParticiones;
} Disco;

typedef struct _IndexDisco {
    char id[32];
    char nombre[32];
    int tamanio;
    int particionesPrimarias;
    int particionesExtendidas;
    int particionesLogicas;
    int espacioSinParticionar;
    int estado;

} IndexDisco;

typedef struct _Particion {
    int estado;
    char nombre[32];
    int inicio;
    int cantBloques;
    int cantBloquesLibres;
    int tipoParticion;
    int tipoAjuste;
    int tipoFormato;
    int tamanio;
    int tamanioBloque;
} Particion;

typedef struct _MBR {
    char nombre[32];
    int tamanio;
    int cantPartiones;
    Particion particion[16];

} MBR;

typedef struct {
    char nombre[16];
    char extension[5];
    char fechaCreacion[16];
    int tamanio;
    int iDInicio;
    int iDFinal;
    char estado;

} Root;

typedef struct {
    int iD;
    char estado;
    int anterior;
    int siguiente;
    int dato;

} Bloque;

int N = 0;

typedef struct {
    char datos[10];

} Fat;


//****Estructuras 2da fase*******
typedef struct {
    char nombre[32];
    int inicio;
    int fin;

} ArchivoEnlazado;

typedef struct {
    int apuntador;
    char contenido[100];

} BloqueEnlazado;


//sistema ext3-segunda fase
typedef struct {
    int numeroInodos;
    int numeroBloques;
    int tamanioBloque;
    int numeroMagico;
    int bloquesLibres;
    int inodosLibres;
    struct tm *fechaMontaje;
    struct tm *fechaDesmontaje;
    int contadorMontados;
    int apunBitMatFicheros;
    int apunBitacora;
    int apunInodo;
    int apunBloque;
    int firstFreeBitMapInodo;
    int firstFreeBitMapBloque;
    int firstFreeInodo;
    int firstFreeBloque;
} SuperBloque;

typedef struct {
    int llave;
    struct tm *fechaMod;
    int tamArchivo;
    char tipo[50];
    struct tm *timeLastAccess;
    struct tm *cTime;
    int directo[5];
    int indirecto[2];

} Inodo;

typedef struct {
    int llave;
    char padre[50];
    char nombre[50];
    char contenido[64];
    int apuntadores[6];

} BloqueExt3;

typedef struct {
    char tipoOperacion[50];
    int tipo;
    char nombre[200];
    char contenido[200];
    struct tm *fecha;

} Log;

//-----------Fin Estructuras--------------
char rutaGeneral[50] = "";
char rutaCarpeta[50] = "";
char archivoExt3[300];
//char contenidoArchivoExt3[100];
int pos = 0;
int it = 0;

int main() {
    int at;
    for(at=0;at<sizeof(archivoExt3);at++){
        archivoExt3[at]='a';
    }
    //for(at=0;at<sizeof(contenidoArchivoExt3);at++){
   //     contenidoArchivoExt3[at]='a';
   // }
    //system("dot -Tpng /home/jose/Documentos/Discos/Disco.dot -o /home/jose/Documentos/Discos/Disco.png");
    //printf("Hello world!\n");
    printf("*************************************\n");
    printf(" Manejo e implementacion de archivos \n");
    printf("*************************************\n");
    printf("Ingrese una ruta para la creacion de discos:\n");
    ///home/jose/Documentos
    char ruta[60];
    //scanf("%s",&ruta);
    strcat(ruta, "/home/jose/Documentos/Discos");
    char index[60] = "";
    strcat(index, ruta);
    strcat(index, "/index.idx");
    strcat(rutaGeneral, index);
    strcat(rutaCarpeta, ruta);
    //printf(rutaGeneral);

    FILE *archivo = fopen(index, "rb+");
    if (archivo == 0) {
        archivo = fopen(index, "wb+");
    }
    fflush(archivo);
    fclose(archivo);

    menu(ruta);
    return 0;
}

void menu(char **ruta) {

    int opcion;

    while (1) {

        printf("1. Crea disco\n");
        printf("2. Aumentar tamanio del disco\n");
        printf("3. Reducir tamanio del disco\n");
        printf("4. Eliminar disco\n");
        printf("5. Crear particion\n");
        printf("6. Eliminar particion\n");
        printf("7. Formatear particion\n");
        printf("8. Crear archivo\n");
        printf("9. Eliminar archivo\n");
        printf("20. Editar archivo\n");
        printf("10. Ver informacion de los discos \n");
        printf("11. Ver informacion de las particiones \n\n");
        printf("********************************\n");
        printf("************REPORTES************\n");
        printf("********************************\n");
        printf("12. Reporte de archivo de discos \n");
        printf("13. Reporte de particion \n");
        printf("14. Reporte de particiones del disco \n");
        printf("15. Reporte del MBR\n");
        printf("********************************\n");
        printf("******Menu Sistema Enlazado*****\n");
        printf("********************************\n");
        printf("16. Cargar archivo csv\n");
        printf("17. Leer archivo csv\n");
        printf("171. Leer bloques archivo csv\n");
        printf("172. Borrar bloque archivo csv\n");
        printf("173. Agregar bloque archivo csv\n");
        printf("********************************\n");
        printf("******Menu Sistema Ext3*****\n");
        printf("********************************\n");
        printf("18. Crear directorio\n");
        printf("19. Eliminar directorio\n");
        printf("20. Listar directorio actual\n");
        printf("21. Listar directorio recursivo\n");
        printf("22. Buscar directorio\n");
        printf("23. Crear archivo en sistema ext3\n");
        printf("24. Leer archivo en sistema ext3\n");
        printf("25. Eliminar archivo en sistema ext3\n");
        printf("26. Editar nombre archivo en sistema ext3\n");
        printf("27. buscar archivo en sistema ext3\n");
        printf("====================================\n");
        printf("==========Reportes fase 2===========\n");
        printf("====================================\n");
        printf("28. Ruta Inodo bloques carpeta\n");
        printf("281. Ruta Inodo bloques archivo\n");
        printf("29. Mostrar bloques archivos\n");
        printf("30. Mostrar bloque seguin ID\n");
        printf("31. Bit map inodos\n");
        printf("32. bit map bloques\n");
        printf("33. reporte bitacora\n");
        printf("174. Super bloque\n");
        printf("********************************\n");
        printf("******FASE3 Menu para crear scripts*****\n");
        printf("********************************\n");
        printf("34. Crear scripts\n");
        printf("\nElija una opcion: ");
        scanf("%d", &opcion);
        switch (opcion) {
            case 1:
                crearDisco(ruta);
                // crear disco
                break;

            case 2:
                aumentarTamanio();
                //aumentar tamanio
                break;
            case 3:
                //reducir mamanio
                reducirEspacio();
                break;

            case 4:
                eliminarDisco();
                break;
            case 5:
                crearParticion();
                break;
            case 6:
                eliminarParticion();
                break;
            case 7:
                formatearParticion();
                break;
            case 8:
                crearArchivo();
                break;
            case 9:
                eliminarArchivo();
                break;
            case 200:
                 modificarArchivo();
                 break;
            case 10:
                informacionDiscos();
                break;
            case 11:
                informacionParticiones();
                break;
            case 12:
                reporteDeArchivosDisco();
                break;
            case 13:
                reporteDeParticion();
                break;
            case 14:
                reporteDeParticionesDelDisco();
                break;
            case 15:
                reporteMBR();
                break;
            case 16:
                cargarCSV();
                break;
            case 17:
                leerArchivoCSV();
                break;
            case 18:
            crearDirectorio();
            break;
        case 19:
            //eliminar directorio
            break;
        case 20:
            //listar directorio actual
            listarDirectorioActual();
            break;
        case 21:
            //listar directorio recursivo
            break;
        case 22:
            //buscar direcorio
            buscarDirectorioArchivo(1);
            break;
            case 23:
                crearArchivoExt3();
            break;
            case 24:
                leerArchivoExt3();
            break;
            case 25:
                //eliminar archivo
                eliminarArchivoExt3();
            break;
            case 26:
                modificarNombreArchivoExt3();
            break;
            case 27:
            buscarDirectorioArchivo(0);
                //buscar archivo
            break;
        case 28:
            //ruta inodo bloquesrutaInodosBloquesArchivo()
            rutaInodosBloques();
        break;
        case 281:
            //ruta inodo bloques
            rutaInodosBloquesArchivo();

        break;
        case 29:
            //bloqes inodo bloques
            BloquesArchivo();
        break;
        case 30:
            //bloques segun id
            BloquesID();
        break;
        case 31:
            //bloques segun id
            BitmapInodos();
        break;
        case 32:
            //bloques segun id
            BitmapBloque();
        break;
        case 33:
            //bloques segun id
            reporteBitacora();
        break;
        case 171:
            //bloques segun id
            mostrarBloquesCSV();
        break;
        case 172:
            //bloques segun id
            borrarBloqueCSV();
        break;
        case 173:
            //bloques segun id
            agregarBloqueCSV();super();
        break;
        case 174:
            //bloques segun id
            super();
            break;
        case 34:
            //bloques segun id
            system("clear");
            printf("*****Crear scripts de particiones enlazadas*****\n\n");
            crearScript();
            break;
            default:
                printf("-----Hasta luego-----\n");
                exit(1);
                break;

        }


    }

    //printf("x-x-x-x-x-Opcion invalida-x-x-x-x-x\n\n");
    //menu(ruta);

}

void crearDisco(char *ruta) {
    Disco disco;
    printf("***NUEVO DISCO***\n");
    printf("Nombre del disco: ");
    scanf("%s", &disco.nombre);
    if (existeDisco(disco.nombre) == 1) {
        printf("-----Disco NO creado por que ya existe otro con el mismo nombre-----\n\n");
        return;
    }
    /*while(existeDisco(disco.nombre)==1){
        printf("-----Ya existe un disco con el mismo nombre-----\n\n");
        printf("Ingrese otro nombre para el disco: ");
        scanf("%s",&disco.nombre);
    }*/

    printf("Tamanio del disco en MB: ");
    scanf("%d", &disco.tamanio);
    //printf(ruta);
    char rutaDisco[60] = "";
    strcat(rutaDisco, ruta);
    strcat(rutaDisco, "/");
    strcat(rutaDisco, disco.nombre);
    strcat(rutaDisco, ".vd");
    //printf(rutaDisco);

    int tamanioDisco = disco.tamanio * 1024 * 1024;

    FILE *archivo = fopen(rutaDisco, "wb+");

    MBR mbr;
    strcpy(mbr.nombre, disco.nombre);
    mbr.tamanio = tamanioDisco;
    mbr.cantPartiones = 0;

    for (it = 0; it < 16; it++) {
        mbr.particion[it].estado = 0;
        mbr.particion[it].cantBloques = 0;
        mbr.particion[it].cantBloquesLibres = 0;
        mbr.particion[it].inicio = -1;
    }

    fwrite(&mbr, sizeof (MBR), 1, archivo);
    int tamanio = sizeof (MBR);

    //printf("incio \n");
    //printf("%d",tamanio);
    //printf("\n");
    //printf("tamanio \n");
    //printf("%d",tamanioDisco);
    int i2;
    for (i2 = tamanio; i2 < tamanioDisco; i2++) {
        fprintf(archivo, "%c", "~");
    }
    fflush(archivo);
    fclose(archivo);

    IndexDisco indexDisco;
    char id = (char) obtenerID();
    stpcpy(indexDisco.id, "vd");
    strcat(indexDisco.id, &id);
    stpcpy(indexDisco.nombre, disco.nombre);
    indexDisco.tamanio = tamanioDisco;
    indexDisco.particionesPrimarias = 0;
    indexDisco.particionesExtendidas = 0;
    indexDisco.particionesLogicas = 0;
    indexDisco.espacioSinParticionar = indexDisco.tamanio - sizeof (MBR);
    indexDisco.estado = 1;

    char rutaIndex[60] = "";
    strcat(rutaIndex, ruta);
    strcat(rutaIndex, "/index.idx");
    //printf(rutaIndex);
    FILE *archivoIndex = fopen(rutaIndex, "ab");
    fwrite(&indexDisco, sizeof (IndexDisco), 1, archivoIndex);
    printf("-----Disco creado exitosamente-----\n\n");
    fflush(archivoIndex);
    fclose(archivoIndex);

}

void eliminarDisco() {
    IndexDisco dis;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");


    //FILE *archivoEliminarI=fopen(rutaGeneral,"rb+");
    char nombreDisco[32];
    printf("Nombre del disco a eliminar\n");
    scanf("%s", &nombreDisco);

    if (existeDisco(nombreDisco) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    char rutaEliminar[50] = "";
    strcat(rutaEliminar, rutaCarpeta);
    strcat(rutaEliminar, "/");
    strcat(rutaEliminar, nombreDisco);
    strcat(rutaEliminar, ".vd");
    //FILE *archivoEliminar=fopen(rutaEliminar,"wb");
    printf("Esta accion Eliminara el disco permanentemente\n");
    printf("1. Eliminar disco\n");
    printf("2. No eliminar disco\n");
    int opcionEliminar;
    scanf("%d", &opcionEliminar);
    printf("opcion eliminar ");
    printf("%d", opcionEliminar);
    printf("\n");
    if (opcionEliminar == 1) {
        remove(rutaEliminar);

        pos = 0;
        while (fread(&dis, sizeof (IndexDisco), 1, archivoIndex) == 1) {

            pos += sizeof (IndexDisco);
            if (!strcmp(nombreDisco, dis.nombre) && dis.estado == 1) {

                fseek(archivoIndex, pos - sizeof (IndexDisco), SEEK_SET);

                dis.estado = 0;
                //strcpy(dis.nombre,"disco modificado");

                fwrite(&dis, sizeof (IndexDisco), 1, archivoIndex);
                printf("-----El disco ");
                printf(nombreDisco);
                printf(" fue eliminado-----");
                printf("\n\n");
                break;
            }

        }
        fflush(archivoIndex);
        fclose(archivoIndex);
        //printf("-----Disco eliminado-----\n");
    } else if (opcionEliminar == 2) {
        printf("-----Disco NO eliminado-----\n");
    } else {
        printf("-----Disco NO eliminado porque no selecciono una opcion-----\n");
    }

}

void aumentarTamanio() {
    printf("ID del disco: ");
    char id[32];
    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    printf("Tamanio a aumentar en MB: ");
    int tamanio;
    scanf("%d", &tamanio);
    //printf("\n");

    IndexDisco disco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");
    IndexDisco discoBuscado;
    int i = 0;
    pos = 0;
    while (fread(&disco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, disco.id) && disco.estado == 1) {
            discoBuscado = disco;
            i = 1;
            break;
        }
        pos += 1;
    }


    if (i == 1) {
        printf("-----Se ha aumentado el tamanio del disco ");
        printf("%s", discoBuscado.nombre);
        printf("-----");
        printf("\n\n");
        char ruta[60] = "";
        strcat(ruta, rutaCarpeta);
        strcat(ruta, "/");
        strcat(ruta, discoBuscado.nombre);
        strcat(ruta, ".vd");
        //printf("%s",ruta);
        FILE *archivo = fopen(ruta, "rb+");
        MBR mbr;
        fread(&mbr, sizeof (MBR), 1, archivo);
        mbr.tamanio = mbr.tamanio + tamanio * 1024 * 1024;
        fseek(archivo, 0, SEEK_SET);
        fwrite(&mbr, sizeof (MBR), 1, archivo);
        fflush(archivo);
        fclose(archivo);
        archivo = fopen(ruta, "ab");

        for (i = 0; i < tamanio * 1024 * 1024; i++) {
            fprintf(archivo, "%c", "~");
        }
        //disco.tamanio += tamanio;
        fseek(archivoIndex, sizeof (IndexDisco) * pos, SEEK_SET);
        disco.tamanio = disco.tamanio + tamanio * 1024 * 1024;
        disco.espacioSinParticionar = +disco.espacioSinParticionar + tamanio * 1024 * 1024;
        fwrite(&disco, sizeof (IndexDisco), 1, archivoIndex);
        fflush(archivo);
        fclose(archivo);

    } else {
        printf("-----El disco no existe-----");
        printf("\n");

    }

    fflush(archivoIndex);
    fclose(archivoIndex);
}

void reducirEspacio() {
    printf("Id del disco: ");
    char id [32];
    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");
    IndexDisco discoBuscado;

    pos = 0;
    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            discoBuscado = indexDisco;
            break;
        }
        pos += 1;
    }


    printf("Espacio a reducir en MB: ");
    int espacio;
    scanf("%d", &espacio);
    if (espacio * 1024 * 1024 > indexDisco.espacioSinParticionar) {
        printf("------No hay suficiente espacion-----\n\n");
        return;
    }
    char ruta[60] = "";
    strcat(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    int espacioNuevo = indexDisco.tamanio - espacio * 1024 * 1024;
    indexDisco.tamanio = espacioNuevo;
    int espacioLibreNuevo = indexDisco.espacioSinParticionar - espacio * 1024 * 1024;
    indexDisco.espacioSinParticionar = espacioLibreNuevo;
    truncate(ruta, espacioNuevo);
    fseek(archivoIndex, sizeof (IndexDisco) * pos, SEEK_SET);
    fwrite(&indexDisco, sizeof (IndexDisco), 1, archivoIndex);
    fflush(archivoIndex);
    fclose(archivoIndex);


    FILE *archivo = fopen(ruta, "rb+");
    MBR mbr;
    fread(&mbr, sizeof (MBR), 1, archivo);
    mbr.tamanio = espacioNuevo;
    fseek(archivo, 0, SEEK_SET);
    fwrite(&mbr, sizeof (MBR), 1, archivo);
    printf("-----Accion completada exitosamente-----\n\n");
    fflush(archivo);
    fclose(archivo);

}

void crearParticion() {
    printf("***Nueva particion***\n");
    printf("ID del disco: ");
    char id[32];
    scanf("%s", &id);

    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n");
        return;
    }


    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");
    IndexDisco discoBuscado;

    pos = 0;
    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            discoBuscado = indexDisco;
            break;
        }
        pos += 1;
    }

    printf("Nombre: ");
    char nombre[32];
    scanf("%s", nombre);
    printf("Tamanio en Kb (minimp 250 Kb): ");
    int tamanioP;
    scanf("%d", &tamanioP);
    if(tamanioP<250){
        printf("El tamanio de la particion no debe ser menor a 250 Kb\n\n");
        return;
    }

    if ((tamanioP * 1024) > indexDisco.espacioSinParticionar) {
        printf("-----No hay suficiente espacio para la particion------\n\n");
        return;
    }
    printf("Tipo particion -> (1=Primaria 2=Extendida 3=Logica): ");

    int tipoP;
    scanf("%d", &tipoP);

    int cantDiscos = indexDisco.particionesExtendidas + indexDisco.particionesPrimarias;
    if (cantDiscos >= 4 && tipoP != 3) {
        printf("-----Ya existen 4 particiones-----\n\n");
        return;
    }


    char ruta[60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, discoBuscado.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    MBR mbr;
    int posP = 0;
    int i = 0;
    fread(&mbr, sizeof (MBR), 1, archivo);



    switch (tipoP) {
        case 1://si el tipo de particion es PRIMARIA

            indexDisco.particionesPrimarias = indexDisco.particionesPrimarias + 1;
            for (i = 0; i < 16; i++) {

                if (mbr.particion[i].estado == 0) {
                    if (mbr.particion[i].inicio == -1) {
                        posP = posP + sizeof (MBR);
                        break;
                    } else {
                        if (mbr.particion[i].tamanio >= tamanioP) {
                            if (mbr.particion[i].tipoParticion == 1 || mbr.particion[i].tipoParticion == 2) {
                                posP = mbr.particion[i].inicio;
                                break;
                            }
                        } else {
                            if (mbr.particion[i].tipoParticion == 1 || mbr.particion[i].tipoParticion == 2) {
                                posP = posP + mbr.particion[i].tamanio * 1024;
                            }
                        }
                    }


                } else {
                    if (mbr.particion[i].tipoParticion == 1 || mbr.particion[i].tipoParticion == 2) {
                        posP = posP + mbr.particion[i].tamanio * 1024;
                    }
                }
            }
            break;
        case 2://si el tipo de particion es EXTENDIDA
            if (indexDisco.particionesExtendidas == 1) {
                printf("-----Ya existe una particion extendida en el disco-----\n\n");
                return;
            }

            for (i = 0; i < 16; i++) {

                if (mbr.particion[i].estado == 0) {
                    if (mbr.particion[i].inicio == -1) {
                        posP = posP + sizeof (MBR);
                        break;
                    } else {
                        if (mbr.particion[i].tamanio >= tamanioP) {
                            if (mbr.particion[i].tipoParticion == 1 || mbr.particion[i].tipoParticion == 2) {
                                posP = mbr.particion[i].inicio;
                                break;
                            }
                        } else {
                            if (mbr.particion[i].tipoParticion == 1 || mbr.particion[i].tipoParticion == 2) {
                                posP = posP + mbr.particion[i].tamanio * 1024;
                            }
                        }
                    }


                } else {
                    if (mbr.particion[i].tipoParticion == 1 || mbr.particion[i].tipoParticion == 2) {
                        posP = posP + mbr.particion[i].tamanio * 1024;
                    }
                }
            }


            indexDisco.particionesExtendidas = 1;
            break;
        case 3://si el tipo de particion es LOGICA
            if (indexDisco.particionesExtendidas == 0) {
                printf("-----No existe una particion extendida-----\n\n");
                return;
            }

            int posExt = obtenerPosExt(&mbr, tamanioP * 1024);
            if (posExt == -1) {
                printf("-----No hay espacion suficiente para la particion-----\n\n");
                return;
            }
            /*
            printf("posicion extendida: ");
            printf("%d",posExt);
            printf("\n");
             */
            indexDisco.particionesLogicas = indexDisco.particionesLogicas + 1;
            for (i = 0; i < 16; i++) {

                if (mbr.particion[i].estado == 0) {
                    if (mbr.particion[i].inicio == -1) {
                        posP = posP + posExt;
                        break;
                    } else {
                        if (mbr.particion[i].tamanio >= tamanioP) {
                            if (mbr.particion[i].tipoParticion == 3) {
                                posP = mbr.particion[i].inicio;
                                break;
                            }
                        } else {
                            if (mbr.particion[i].tipoParticion == 3) {
                                posP = posP + mbr.particion[i].tamanio * 1024;
                            }
                        }
                    }


                } else {
                    if (mbr.particion[i].tipoParticion == 3) {
                        posP = posP + mbr.particion[i].tamanio * 1024;
                    }
                }
            }


            break;
        default:

            break;
    }

    int tipoF;
    printf("Tipo Sistema de archivos -> (1=FAT 2=Enlazado 3=Ext3): ");

    scanf("%d", &tipoF);

   /* if(tipoS==3){
        //sistemaExt3();
        fflush(archivo);
        fclose(archivo);
        fflush(archivoIndex);
        fclose(archivoIndex);
        return;
    }*/


    int tipoA;
    int tamanioB;
    if ((tipoP == 1 || tipoP == 3)&&tipoF!=3) {
        printf("Tipo ajuste -> (1=Primer ajuste 2=Mejor ajuste 3=Peor ajuste): ");

        scanf("%d", &tipoA);
        if(tipoF==1){
        printf("Tamanio de los bloques en B (minimo 128 B): ");

        scanf("%d", &tamanioB);

        if(tamanioB<128){
            printf("El tamanio del bloque no debe de ser menor a 128\n\n");
            return;
        }
        }

    } else {
        tipoA = 1;
        //tamanioB=10000;
    }

    //se modifica la informacion del index
    fseek(archivoIndex, pos * sizeof (IndexDisco), SEEK_SET);
    if (tipoP != 3) {
        indexDisco.espacioSinParticionar = indexDisco.espacioSinParticionar - tamanioP * 1024;
    }
    fwrite(&indexDisco, sizeof (IndexDisco), 1, archivoIndex);



    //while(mbr.particion[i].estado==1){

    //}

    //posP += sizeof (MBR);
    mbr.particion[i].estado = 1;
    mbr.particion[i].tipoFormato=tipoF;
    strcpy(mbr.particion[i].nombre, nombre);
    mbr.particion[i].inicio = posP;
    mbr.cantPartiones = mbr.cantPartiones + 1;
    mbr.particion[i].tipoAjuste = tipoA;
    mbr.particion[i].tipoParticion = tipoP;
    mbr.particion[i].tamanio = tamanioP;
    mbr.particion[i].tamanioBloque = tamanioB - sizeof (Bloque);
    int cantBloques;


    if ((tipoP == 1 || tipoP == 3)&&tipoF==1) {

        int tamRoot = sizeof (Root);
        cantBloques = (tamanioP * 1024 - tamRoot * 256) / (1 + tamanioB);
        mbr.particion[i].cantBloques = cantBloques;
        mbr.particion[i].tipoFormato=tipoF;
        mbr.particion[i].cantBloquesLibres = cantBloques;
        fseek(archivo, 0, SEEK_SET);
        fwrite(&mbr, sizeof (MBR), 1, archivo);


        fseek(archivo, posP, SEEK_SET);
        //agrega la fat
        char b = '0';
        for (it = 0; it < cantBloques; it++) {

            fwrite(&b, 1, 1, archivo);
        }
        posP += cantBloques;
        fseek(archivo, posP, SEEK_SET);
        //agrega la root
        for (it = 0; it < 256; it++) {
            Root root;
            root.estado = 0;
            root.iDInicio=-1;
            root.iDFinal=-1;
            strcpy(root.extension,"---");
            strcpy(root.fechaCreacion,"00-00-0000");
            strcpy(root.nombre,"no creado");
            root.tamanio=0;

            fwrite(&root, sizeof (Root), 1, archivo);
        }
        posP += sizeof (Root)*256;
        fseek(archivo, posP, SEEK_SET);
        //agrega bloques a la particion
        for (it = 0; it < cantBloques; it++) {
            Bloque bloque;
            bloque.iD = it;
            //bloque.estado = "0";
            bloque.estado='0';
            //strcpy(bloque.estado,"0");
            int tamSN=sizeof (Bloque);
            bloque.dato = tamanioB -tamSN;

            bloque.anterior = -1;
            bloque.siguiente = -1;

            fwrite(&bloque, sizeof (Bloque), 1, archivo);
            char *datoBloque = (char *) malloc(bloque.dato);
            strcpy(datoBloque, "--");
            fwrite(datoBloque, bloque.dato, 1, archivo);
            //bloque.dato = malloc(tamanioB-13);
            //bloque.dato = malloc(sizeof(char*)*50);

        }
    }
    else if((tipoP == 1 || tipoP == 3)&&tipoF==2){
        cantBloques=(tamanioP*1024-sizeof(ArchivoEnlazado))/100;
        mbr.particion[i].cantBloques = cantBloques;
        mbr.particion[i].cantBloquesLibres = cantBloques;
        mbr.particion[i].tipoFormato=tipoF;
        fseek(archivo, 0, SEEK_SET);
        fwrite(&mbr, sizeof (MBR), 1, archivo);
        sistemaEnlazado(archivo,&mbr.particion[i],mbr.particion[i].cantBloques);
    }else if((tipoP == 1 || tipoP == 3)&&tipoF==3){

        cantBloques=(tamanioP*1024-sizeof(SuperBloque))/(2+sizeof(BloqueExt3)+sizeof(Inodo)+100*sizeof(Log));
        mbr.particion[i].cantBloques = cantBloques;
        mbr.particion[i].tipoFormato=tipoF;
        mbr.particion[i].cantBloquesLibres = cantBloques;
        fseek(archivo, 0, SEEK_SET);
        fwrite(&mbr, sizeof (MBR), 1, archivo);
        sistemaExt3(archivo,&mbr.particion[i],mbr.particion[i].cantBloques);
    }else if(tipoP==2){
        //mbr.particion[i].cantBloquesLibres = cantBloques;
        fseek(archivo, 0, SEEK_SET);
        fwrite(&mbr, sizeof (MBR), 1, archivo);
    }


    printf("\n");
    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);



}

void eliminarParticion() {
    printf("Id del disco: ");
    char id[32];
    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }


    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    pos = 0;
    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
        pos += 1;
    }

    printf("Nombre de la particion: ");
    char nombre[32];
    scanf("%s", &nombre);
    if (existeParticion(nombre, indexDisco.nombre) == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    }


    MBR mbr;
    char ruta[60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    if (modificarEstado(&mbr, &indexDisco, ruta, nombre) == 0) {
        printf("-----La particion no existe-----\n\n");
        fflush(archivo);
        fclose(archivo);
        fflush(archivoIndex);
        fclose(archivoIndex);
        return;
    }
    fseek(archivoIndex, sizeof (IndexDisco) * pos, SEEK_SET);
    fwrite(&indexDisco, sizeof (IndexDisco), 1, archivoIndex);

    fseek(archivo, 0, SEEK_SET);
    fwrite(&mbr, sizeof (MBR), 1, archivo);



    printf("-----Accion completada exitosamente-----\n\n");
    fflush(archivo);
    fclose(archivo);
    fflush(archivoIndex);
    fclose(archivoIndex);
}

int modificarEstado(MBR *mbr, IndexDisco *indexDisco, char ruta[], char nombre []) {
    int existe = 0;
    FILE *archivo = fopen(ruta, "rb+");

    for (it = 0; it < 16; it++) {
        //int b=strcmp(nombre,mbr->particion[it].nombre);
        if (!strcmp(nombre, mbr->particion[it].nombre) && mbr->particion[it].estado == 1) {
            mbr->particion[it].estado = 0;
            indexDisco->espacioSinParticionar = indexDisco->espacioSinParticionar + mbr->particion[it].tamanio * 1024;
            existe = 1;
            if (mbr->particion[it].tipoParticion == 1) {
                indexDisco->particionesPrimarias = indexDisco->particionesPrimarias - 1;
            } else if (mbr->particion[it].tipoParticion == 2) {
                indexDisco->particionesExtendidas = indexDisco->particionesExtendidas - 1;
            } else if (mbr->particion[it].tipoParticion == 3) {
                indexDisco->particionesLogicas = indexDisco->particionesLogicas - 1;
            }
            mbr->cantPartiones = mbr->cantPartiones - 1;

            break;
        }
    }

    if (mbr->particion[it].tipoParticion == 2) {
        int i;
        for (i = 0; i < 16; i++) {
            if (mbr->particion[i].tipoParticion == 3 && mbr->particion[i].estado == 1) {
                mbr->particion[i].estado = 0;
                indexDisco->particionesLogicas = indexDisco->particionesLogicas - 1;
                mbr->cantPartiones = mbr->cantPartiones - 1;
            }
        }
    }

    //reajusta las particiones
    int j;
    int tam = 0;
    if (mbr->particion[it].tipoParticion == 1 || mbr->particion[it].tipoParticion == 2) {
        for (j = it + 1; j < 16; j++) {
            if (mbr->particion[j].estado == 1 && (mbr->particion[j].tipoParticion == 1 || mbr->particion[j].tipoParticion == 2)) {
                tam = tam + mbr->particion[j].tamanio * 1024;
                //mbr->particion[j].inicio=mbr->particion[j].inicio-mbr->particion[it].tamanio*1024;
            }
        }
        char *dato = (char *) malloc(tam);
        int ini = mbr->particion[it].inicio + mbr->particion[it].tamanio * 1024;
        fseek(archivo, ini, SEEK_SET);
        fread(dato, tam, 1, archivo);
        //int ini=mbr->particion[it].inicio+mbr->particion[it].tamanio*1024;
        fseek(archivo, mbr->particion[it].inicio, SEEK_SET);
        fwrite(dato, tam, 1, archivo);

    } else if (mbr->particion[it].tipoParticion == 3) {
        for (j = it + 1; j < 16; j++) {
            if (mbr->particion[j].estado == 1 && mbr->particion[j].tipoParticion == 3) {
                tam = tam + mbr->particion[j].tamanio * 1024;
                //mbr->particion[j].inicio=mbr->particion[j].inicio-mbr->particion[it].tamanio*1024;
            }
        }

        char *dato = (char *) malloc(tam);
        int ini = mbr->particion[it].inicio + mbr->particion[it].tamanio * 1024;
        fseek(archivo, ini, SEEK_SET);
        fread(dato, tam, 1, archivo);

        fseek(archivo, mbr->particion[it].inicio, SEEK_SET);
        fwrite(dato, tam, 1, archivo);
    }



    if (mbr->particion[it].tipoParticion == 1 || mbr->particion[it].tipoParticion == 2) {
        for (j = it + 1; j < 16; j++) {
            if (mbr->particion[j].estado == 1 && (mbr->particion[j].tipoParticion == 1 || mbr->particion[j].tipoParticion == 2 || mbr->particion[j].tipoParticion == 3)) {
                //tam=tam+mbr->particion[j].tamanio*1024;
                mbr->particion[j].inicio = mbr->particion[j].inicio - mbr->particion[it].tamanio * 1024;
            }
        }

    } else if (mbr->particion[it].tipoParticion == 3) {
        for (j = it + 1; j < 16; j++) {
            if (mbr->particion[j].estado == 1 && mbr->particion[j].tipoParticion == 3) {
                //tam=tam+mbr->particion[j].tamanio*1024;
                mbr->particion[j].inicio = mbr->particion[j].inicio - mbr->particion[it].tamanio * 1024;
            }
        }

    }



    int cantP = 0;
    MBR *mbrAux = mbr;
    for (j = 0; j < 16; j++) {


        if (mbrAux->particion[j].estado == 1) {

            mbr->particion[cantP] = mbr->particion[j];
            cantP = cantP + 1;
        }

    }


    if (cantP < 16) {
        int j;
        for (j = cantP; j < 16; j++) {
            mbr->particion[j].estado = 0;
            mbr->particion[j].cantBloques = 0;
            mbr->particion[j].cantBloquesLibres = 0;
            mbr->particion[j].inicio = -1;
            //mbr->particion[j].tamanio=0;
            //mbr->particion[j].tipoAjuste=0;
            //mbr->particion[j].tamanioBloque=0;
            //mbr->particion[j].tipoParticion=0;

        }
    }
    fflush(archivo);
    fclose(archivo);

    //fin del reajuste

    return existe;
}

void formatearParticion() {
    printf("Id del disco: ");
    char id [32];
    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }
    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }

    printf("Nombre de la particion: ");
    char nombre[32];
    scanf("%s", &nombre);
    int formatear = existeParticion13(nombre, indexDisco.nombre);
    if (formatear == 0) {
        printf("-----La particion no existe\n\n");
        return;
    } else if (formatear == -1) {
        printf("-----Una particion extendida no se puede formatear-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int i;
    for (i = 0; i < 16; i++) {
        if (!strcmp(nombre, mbr.particion[i].nombre)) {
            break;
        }
    }

    printf("1. Formateo rapido\n");
    printf("2. Formateo completo\n");
    printf("otra cosa para ir al menu principal\n\n");
    printf("Elija una opcion: ");
    int opcion;
    scanf("%d", &opcion);
    if (opcion == 1) {
        formateoRapido(indexDisco.nombre, nombre);
        printf("\n\n");
    } else if (opcion == 2) {
        printf("\n---Elija un sistema de archivos---\n");
        printf("1. Sistema de archivos FAT\n");
        printf("2. Sistema de archivos Enlazado\n");
        printf("3. Sistema de archivos Ext3\n");
        printf("otra cosa para ir al menu principal\n\n");
        printf("Elija una opcion: ");
        int opcionSistema;
        scanf("%d", &opcionSistema);
        switch (opcionSistema) {
        case 1:
            formateoCompleto(indexDisco.nombre, nombre);
            break;
        case 2:
            sistemaEnlazado(archivo,&mbr.particion[i],mbr.particion[i].cantBloques);
            mbr.particion[i].tipoFormato=opcionSistema;
            fseek(archivo, 0, SEEK_SET);
            fwrite(&mbr, sizeof (MBR), 1, archivo);
            printf("\n---Particion Formateada\n");
            break;
        case 3:
            sistemaExt3(archivo,&mbr.particion[i],mbr.particion[i].cantBloques);
            mbr.particion[i].tipoFormato=opcionSistema;
            fseek(archivo, 0, SEEK_SET);
            fwrite(&mbr, sizeof (MBR), 1, archivo);
            printf("\n---Particion Formateada\n");
            break;
        default:
            printf("\n\n");
        }

        printf("\n\n");
    } else {
        printf("\n\n");
    }




    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);

}

void formateoRapido(char *nombre, char *nombreP) {
    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, nombre);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int i;
    for (i = 0; i < 16; i++) {
        if (!strcmp(nombreP, mbr.particion[i].nombre)) {
            break;
        }
    }
    //modifica la fat
    fseek(archivo, mbr.particion[i].inicio, SEEK_SET);
    char b = '0';
    for (it = 0; it < mbr.particion[i].cantBloques; it++) {

        fwrite(&b, 1, 1, archivo);
    }

    //modifica la root
    for (it = 0; it < 256; it++) {
        Root root;
        root.estado = 0;
        fwrite(&root, sizeof (Root), 1, archivo);
    }

    fflush(archivo);
    fclose(archivo);
}

void formateoCompleto(char *nombre, char *nombreP) {
    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, nombre);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int i;
    for (i = 0; i < 16; i++) {
        if (!strcmp(nombreP, mbr.particion[i].nombre)) {
            break;
        }
    }
    //modifica la fat
    fseek(archivo, mbr.particion[i].inicio, SEEK_SET);
    char b = '0';
    for (it = 0; it < mbr.particion[i].cantBloques; it++) {

        fwrite(&b, 1, 1, archivo);
    }

    //modifica la root
    for (it = 0; it < 256; it++) {
        Root root;
        root.estado = 0;
        fwrite(&root, sizeof (Root), 1, archivo);
    }

    for (it = 0; it < mbr.particion[i].cantBloques; it++) {
        Bloque bloque;
        bloque.iD = it;
        bloque.estado = '0';
        //bloque.estado=0;
        bloque.dato = mbr.particion[i].tamanioBloque - sizeof (Bloque);

        bloque.anterior = -1;
        bloque.siguiente = -1;

        fwrite(&bloque, sizeof (Bloque), 1, archivo);

        char *datoBloque = (char *) malloc(bloque.dato);
        strcpy(datoBloque, "bloquevacio");
        fwrite(datoBloque, bloque.dato, 1, archivo);

    }

    fflush(archivo);
    fclose(archivo);
}

void crearArchivo() {
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    char nombreArchivo[21];
    printf("Nombre del archivo: ");
    scanf("%s", &nombreArchivo);

    char soloNombre[16] = "";
    char extension [15] = "";
    int i;
    int p = 0;
    int posExtencion = 0;
    for (i = 0; i<sizeof (nombreArchivo); i++) {
        char c = nombreArchivo[i];
        if (c != '.' && p == 0) {
            soloNombre[i] = c;
        } else {
            p = 1;
        }
        if (p == 1) {
            if ((c >= 97 && c <= 122) || c == '.') {
                if (c != '.') {
                    extension[posExtencion] = c;
                    posExtencion += 1;
                }
            } else {
                break;
            }

        }
    }

    char c[10000]="";
    char texto[100000]="";
    printf("Ingrese el comando echo:term en una linea nueva para terminar de escribir el archivo\n");
    printf("Ingrese el texto:\n");

    do{

        scanf(" %[^\n]",&c);


        if(!strcmp(c,"echo:term")){
            break;
        }else{
            strcat(texto,c);
            strcat(texto,"\n");
        }
    }while(1);

    int posA=posicionNuevoArchivo(archivo,&mbr,z);
    int posicionVacia=mbr.particion[z].inicio+mbr.particion[z].cantBloques+posA*sizeof(Root);

    Root root;


    //obtiene la fecha del sistema
    time_t tiempo = time(0);
    struct tm *tlocal = localtime(&tiempo);
    char output[128];
    strftime(output,128,"%d/%m/%y",tlocal);
    printf("%s\n",output);
    int tamAr=strlen(texto);
    int cantBloques=ceil((float)(strlen(texto)-1)/(float)(mbr.particion[z].tamanioBloque));

    root.estado=1;

    strcpy(root.extension,extension);
    strcpy(root.nombre,soloNombre);
    strcpy(root.fechaCreacion,output);
    root.tamanio=strlen(texto);
    //root.iDInicio=0;//pendiente
    //root.iDFinal=0;//pendiente

    switch (mbr.particion[z].tipoAjuste) {
    case 1:
        primerAjuste(archivo,&mbr,&root,z,cantBloques);
        llenarBloques(archivo,&mbr,&root,z,cantBloques,texto);
        break;
    case 2:
        mejorAjuste(archivo,&mbr,&root,z,cantBloques);
        llenarBloques(archivo,&mbr,&root,z,cantBloques,texto);
        break;
    case 3:
        peorAjuste(archivo,&mbr,&root,z,cantBloques);
        llenarBloques(archivo,&mbr,&root,z,cantBloques,texto);

        break;
    }
    fseek(archivo,posicionVacia,SEEK_SET);
    fwrite(&root,sizeof(Root),1,archivo);
    fseek(archivo,0,SEEK_SET);
    fwrite(&mbr,sizeof(MBR),1,archivo);

    fflush(archivo);
    fclose(archivo);
    fflush(archivoIndex);
    fclose(archivoIndex);

}

int posicionNuevoArchivo(FILE **archivo,MBR *mbr,int z){
    int pos;

    fseek(archivo,mbr->particion[z].inicio+mbr->particion[z].cantBloques,SEEK_SET);
    Root root;
    int i;
    for(i=0;i<256;i++){
        fread(&root,sizeof(Root),1,archivo);
        char nom[32];
        strcpy(nom,root.nombre);
        printf("\nnombre archivo: ");
        printf("%s",nom);
        printf("\nextencion archivo: ");
        printf("%s",root.extension);
        printf("\n");
        if(root.estado==0){
            pos=i;
            break;
        }
    }

    return pos;
}

int mejorAjuste(FILE **archivo,MBR *mbr,Root *root,int z,int cantBloques){
    fseek(archivo,mbr->particion[z].inicio,SEEK_SET);
    int i;
    int inicio=0;
    int final=0;
    int contador=0;
    for(i=0;i<mbr->particion[z].cantBloques;i++){

        char c;
        fread(&c,1,1,archivo);
        if(c=='0'){
            contador+=1;
        }else{
            contador=0;
        }
        if(contador==cantBloques){
            final=i;
            inicio=(i+1)-contador;
            break;
        }
    }

    if(contador==cantBloques){
        root->iDFinal=final;
        root->iDInicio=inicio;
        mbr->particion[z].cantBloquesLibres=mbr->particion[z].cantBloquesLibres-cantBloques;

        fseek(archivo,mbr->particion[z].inicio+inicio,SEEK_SET);
        int j;
        char c='1';
        for(j=0;j<cantBloques;j++){
            fwrite(&c, 1, 1, archivo);
        }

        return 1;
    }else{
        return 0;
    }

}


int primerAjuste(FILE **archivo,MBR *mbr,Root *root,int z,int cantBloques){
    fseek(archivo,mbr->particion[z].inicio,SEEK_SET);
    int i;
    int inicio=0;
    int final=0;
    int contador=0;
    for(i=0;i<mbr->particion[z].cantBloques;i++){

        char c;
        fread(&c,1,1,archivo);
        if(c=='0'){
            contador+=1;
        }else{
            contador=0;
        }
        if(contador==cantBloques){
            final=i;
            inicio=(i+1)-contador;
            break;
        }
    }

    if(contador==cantBloques){
        root->iDFinal=final;
        root->iDInicio=inicio;
        mbr->particion[z].cantBloquesLibres=mbr->particion[z].cantBloquesLibres-cantBloques;

        fseek(archivo,mbr->particion[z].inicio+inicio,SEEK_SET);
        int j;
        char c='1';
        for(j=0;j<cantBloques;j++){
            fwrite(&c, 1, 1, archivo);
        }

        return 1;
    }else{
        return 0;
    }

}

int peorAjuste(FILE **archivo,MBR *mbr,Root *root,int z,int cantBloques){
    fseek(archivo,mbr->particion[z].inicio,SEEK_SET);
    int i;
    int inicio=0;
    int final=0;
    int contador=0;
    for(i=0;i<mbr->particion[z].cantBloques;i++){

        char c;
        fread(&c,1,1,archivo);
        if(c=='0'){
            contador+=1;
        }else{
            contador=0;
        }
        if(contador==cantBloques){
            final=i;
            inicio=(i+1)-contador;
            break;
        }
    }

    if(contador==cantBloques){
        root->iDFinal=final;
        root->iDInicio=inicio;
        mbr->particion[z].cantBloquesLibres=mbr->particion[z].cantBloquesLibres-cantBloques;

        fseek(archivo,mbr->particion[z].inicio+inicio,SEEK_SET);
        int j;
        char c='1';
        for(j=0;j<cantBloques;j++){
            fwrite(&c, 1, 1, archivo);
        }

        return 1;
    }else{
        return 0;
    }

}

void llenarBloques(FILE **archivo,MBR *mbr,Root *root,int z,int cantBloques,char *texto){
    int i;
    int contador=0;
    int contadorB=root->iDInicio;
    int aux=0;
    int des=mbr->particion[z].inicio+mbr->particion[z].cantBloques+256*sizeof(Root)+root->iDInicio*(sizeof(Bloque)+mbr->particion[z].tamanioBloque);
    fseek(archivo,des,SEEK_SET);
    int tam=mbr->particion[z].tamanioBloque;

    char cadenaBloque[tam];
    char pru[10];
    //strcpy(cadenaBloque,"");

    for(i=0;i<(strlen(texto)-1);i++){

     //   if((i+1)==strlen(texto)){
            //fseek(archivo,mbr->particion[z].tamanioBloque,SEEK_CUR);

      //  }else{
               // char ca=texto[i];
                cadenaBloque[contador]=texto[i];
               // pru[contador]=texto[i];
               // int si=strlen(cadenaBloque);
                contador=contador+1;


      //  }

        if(contador==tam){
            //fseek(archivo,sizeof(Bloque),SEEK_CUR);

            Bloque bloque;

            bloque.estado='1';
            bloque.iD=contadorB;
            if(aux==0){
                bloque.anterior=-1;
            }else{
                bloque.anterior=contadorB-1;
                //bloque.siguiente=contadorB+1;
            }
            bloque.siguiente=contadorB+1;

            fwrite(&bloque,sizeof(Bloque),1,archivo);
            int k;
            for(k=(strlen(cadenaBloque)-1);k<mbr->particion[z].tamanioBloque;k++){
                cadenaBloque[k]='\0';
            }
            char ca[1000];
            strcpy(ca,cadenaBloque);
            fwrite(&cadenaBloque,sizeof(cadenaBloque),1,archivo);
            contador=0;
            strcpy(cadenaBloque,"");
            aux++;
            contadorB=contadorB+1;
            int j;
            for(j=0;j<tam;j++){
                cadenaBloque[j]='\0';
            }
        }
    }

    int k;
    for(k=(strlen(cadenaBloque)-1);k<mbr->particion[z].tamanioBloque;k++){
        cadenaBloque[k]='\0';
    }
    Bloque bloque;

    bloque.estado='1';
    bloque.anterior=contadorB-1;
    bloque.siguiente=-1;
    bloque.iD=contadorB;
    fwrite(&bloque,sizeof(Bloque),1,archivo);
    fwrite(&cadenaBloque,tam,1,archivo);


}

void eliminarArchivo(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    char nombreArchivo[21];
    printf("Nombre del archivo: ");
    scanf("%s", &nombreArchivo);

    char soloNombre[16] = "";
    char extension [15] = "";
    int i;
    int p = 0;
    int posExtencion = 0;
    for (i = 0; i<sizeof (nombreArchivo); i++) {
        char c = nombreArchivo[i];
        if (c != '.' && p == 0) {
            soloNombre[i] = c;
        } else {
            p = 1;
        }
        if (p == 1) {
            if ((c >= 97 && c <= 122) || c == '.') {
                if (c != '.') {
                    extension[posExtencion] = c;
                    posExtencion += 1;
                }
            } else {
                break;
            }

        }
    }

    Root root;
    fseek(archivo,mbr.particion[z].inicio+mbr.particion[z].cantBloques,SEEK_SET);
    int j;
    for(j=0;j<256;j++){
        fread(&root,sizeof(Root),1,archivo);
        if(root.estado==1){
            if(!strcmp(soloNombre,root.nombre)&&!strcmp(extension,root.extension)){
                break;
            }
        }
    }


    fseek(archivo,mbr.particion[z].inicio+root.iDInicio,SEEK_SET);

    int fa;
    char c='0';
    for(fa=root.iDInicio;fa<=root.iDFinal;fa++){
        fwrite(&c,1,1,archivo);
    }
    root.estado=0;
    //strcpy(root.nombre,"elimi");
    fseek(archivo,mbr.particion[z].inicio+mbr.particion[z].cantBloques+j*sizeof(Root),SEEK_SET);
    fwrite(&root,sizeof(Root),1,archivo);

    fflush(archivo);
    fclose(archivo);
    fflush(archivoIndex);
    fclose(archivoIndex);


}

void modificarArchivo(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    char nombreArchivo[21];
    printf("Nombre del archivo: ");
    scanf("%s", &nombreArchivo);

    char soloNombre[16] = "";
    char extension [15] = "";
    int i;
    int p = 0;
    int posExtencion = 0;
    for (i = 0; i<sizeof (nombreArchivo); i++) {
        char c = nombreArchivo[i];
        if (c != '.' && p == 0) {
            soloNombre[i] = c;
        } else {
            p = 1;
        }
        if (p == 1) {
            if ((c >= 97 && c <= 122) || c == '.') {
                if (c != '.') {
                    extension[posExtencion] = c;
                    posExtencion += 1;
                }
            } else {
                break;
            }

        }
    }

    Root root;
    int existe=0;
    fseek(archivo,mbr.particion[z].inicio+mbr.particion[z].cantBloques,SEEK_SET);
    int j;
    for(j=0;j<256;j++){
        fread(&root,sizeof(Root),1,archivo);
        if(root.estado==1){
            if(!strcmp(soloNombre,root.nombre)&&!strcmp(extension,root.extension)){
                existe=1;
                break;
            }
        }
    }

    printf("Desea renombrar el archivo: \n");
    printf("1. Si \n");
    printf("1. No \n");
    printf(">> ");
    int opR;
    scanf("%d",&opR);

    if(opR==1){
        printf("Nuevo nombre: ");
        char N[32];
        scanf("%s",N);

        char soloNombre2[16] = "";
        char extension2 [15] = "";
        int p2 = 0;
        int posExtencion2 = 0;
        for (i = 0; i<sizeof (N); i++) {
            char c = N[i];
            if (c != '.' && p2 == 0) {
                soloNombre2[i] = c;
            } else {
                p2 = 1;
            }
            if (p2 == 1) {
                if ((c >= 97 && c <= 122) || c == '.') {
                    if (c != '.') {
                        extension2[posExtencion2] = c;
                        posExtencion2 += 1;
                    }
                } else {
                    break;
                }

            }
        }


        strcpy(root.nombre,soloNombre2);
        strcpy(root.extension,extension2);
        fseek(archivo,mbr.particion[z].inicio+mbr.particion[z].cantBloques+j*sizeof(Root),SEEK_SET);
        fwrite(&root,sizeof(Root),1,archivo);
    }

    if(existe==0){
        printf("-----El archivo no existe-----\n\n");
        return;
    }

    char *cadena=(char *)malloc((root.iDFinal-root.iDInicio)*mbr.particion[z].tamanioBloque);
    fseek(archivo,mbr.particion[z].inicio+mbr.particion[z].cantBloques+256*sizeof(Root)+root.iDInicio*(sizeof(Bloque)+mbr.particion[z].tamanioBloque),SEEK_SET);
    int bl;
    strcpy(cadena,"");
    for(bl=root.iDInicio;bl<=root.iDFinal;bl++){
        Bloque b;
        fread(&b,sizeof(Bloque),1,archivo);
        //fseek(archivo,sizeof(Bloque),SEEK_CUR);
        char *ca=(char *)malloc(mbr.particion[z].tamanioBloque);
        fread(ca,mbr.particion[z].tamanioBloque,1,archivo);
        strcat(cadena,ca);

    }

    printf("===Texto del archivo===\n");
    printf("%s",cadena);
    //char *nano=(char *)malloc(sizeof(cadena)+4);
    //strcpy(nano,"nano ");
    //strcat(nano,cadena);
    //system(nano);

    printf("Desea modificar el contenido del archivo: \n");
    printf("1. Si \n");
    printf("1. No \n");
    printf(">> ");

    scanf("%d",&opR);
    if(opR==1){
    printf("\nIngrese el comando echo:term en una linea nueva para terminar de escribir el archivo\n");
    printf("Ingrese el nuevo texto:\n");

    char c[10000]="";
    char texto[100000]="";
    fflush(stdin);
    do{

        scanf(" %[^\n]",&c);


        if(!strcmp(c,"echo:term")){
            break;
        }else{
            strcat(texto,c);
            strcat(texto,"\n");
        }
    }while(1);
    int cantBloques=ceil((float)(strlen(texto)-1)/(float)(mbr.particion[z].tamanioBloque));
    //primerAjuste(archivo,&mbr,&root,z,cantBloques);
    llenarBloques(archivo,&mbr,&root,z,cantBloques,texto);
    }
    fread(&mbr,sizeof(MBR),1,archivo);
    printf("\n\n");
    //fflush(archivo);
    //fclose(archivo);
    fflush(archivoIndex);
    fclose(archivoIndex);
}





int obtenerPosExt(MBR *mbr, int *tamP) {
    int i;
    int pos = 0;
    int ocupado = 0;
    int tamanio = 0;
    for (i = 0; i < 16; i++) {

        if (mbr->particion[i].tipoParticion == 2) {
            pos = mbr->particion[i].inicio;
            tamanio = mbr->particion[i].tamanio * 1024;
        } else if (mbr->particion[i].tipoParticion == 3 && mbr->particion[i].estado == 1) {
            ocupado = ocupado + mbr->particion[i].tamanio * 1024;
        }

    }

    int libre = tamanio - ocupado;
    if (tamP <= libre) {
        return pos;
    } else {
        return -1;
    }

}

int obtenerID() {
    //de 97 a 122
    FILE *archivoIndex = fopen(rutaGeneral, "rb");

    int i = 0;
    IndexDisco index;

    while (fread(&index, sizeof (IndexDisco), 1, archivoIndex)) {
        i += 1;
    }
    fflush(archivoIndex);
    fclose(archivoIndex);
    i = i + 97;
    return i;
}

int existeDisco(char **nombre) {
    IndexDisco disco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb");
    int i = 0;
    while (fread(&disco, sizeof (IndexDisco), 1, archivoIndex)) {
        //pos += 1;
        if (!strcmp(nombre, disco.nombre)) {
            i = 1;
            break;
        }
    }
    fflush(archivoIndex);
    fclose(archivoIndex);
    return i;
}

int existeDiscoID(char *id) {
    IndexDisco disco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb");
    int i = 0;
    while (fread(&disco, sizeof (IndexDisco), 1, archivoIndex)) {
        //pos += 1;
        if (!strcmp(id, disco.id) && disco.estado == 1) {
            i = 1;
            break;
        }
    }
    fflush(archivoIndex);
    fclose(archivoIndex);
    return i;
}

int existeParticion(char *nombre, char *nombreD) {
    int existe = 0;
    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, nombreD);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int i;
    for (i = 0; i < 16; i++) {
        if (!strcmp(nombre, mbr.particion[i].nombre)) {
            return 1;
        }
    }
    fflush(archivo);
    fclose(archivo);
    return existe;
}

int existeParticion13(char *nombre, char *nombreD) {
    int existe = 0;
    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, nombreD);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int i;
    for (i = 0; i < 16; i++) {
        if (!strcmp(nombre, mbr.particion[i].nombre)) {
            if (nombre, mbr.particion[i].tipoParticion == 2) {
                return-1;
            } else {
                return 1;
            }

        }
    }
    fflush(archivo);
    fclose(archivo);
    return existe;
}


int existeParticionExt3(char *nombre, char *nombreD) {
    int existe = 0;
    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, nombreD);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int i;
    for (i = 0; i < 16; i++) {
        if (!strcmp(nombre, mbr.particion[i].nombre)) {
            if (nombre, mbr.particion[i].tipoParticion == 2) {
                return-1;
            } else {
                if(mbr.particion[i].tipoFormato==3){
                    return 3;
                }else{
                return 1;
                }
            }

        }
    }
    fflush(archivo);
    fclose(archivo);
    return existe;
}

void informacionDiscos() {
    FILE *archivoIndex = fopen(rutaGeneral, "rb");
    IndexDisco index;

    while (fread(&index, sizeof (IndexDisco), 1, archivoIndex)) {
        printf("------------------------------------\n");
        printf("ID: ");
        printf("%s", index.id);
        printf("\n");
        printf("Nombre: ");
        printf("%s", index.nombre);
        printf("\n");
        printf("Tamanio : ");
        int tam = index.tamanio / (1024 * 1024);
        printf("%d", index.tamanio / (1024 * 1024));
        printf(" MB -> ");
        printf("%d", index.tamanio / 1024);
        printf(" Kb -> ");
        printf("%d", index.tamanio);
        printf(" B");
        printf("\n");
        printf("Particiones primarias: ");
        printf("%d", index.particionesPrimarias);
        printf("\n");
        printf("Particiones extendidas: ");
        printf("%d", index.particionesExtendidas);
        printf("\n");
        printf("Particiones logicas: ");
        printf("%d", index.particionesLogicas);
        printf("\n");
        printf("Espacio sin particionar : ");
        tam = index.espacioSinParticionar / (1024 * 1024);
        printf("%d", tam);
        printf(" MB-> ");
        tam = index.espacioSinParticionar / 1024;
        printf("%d", tam);
        printf(" Kb-> ");
        printf("%d", index.espacioSinParticionar);
        printf(" B");
        printf("\n");
        printf("Estado: ");
        printf("%d", index.estado);
        printf("\n");
        printf("------------------------------------\n");
    }
    fflush(archivoIndex);
    fclose(archivoIndex);
}

void informacionParticiones() {
    char nombreDisco[32];
    printf("Nombre del disco: ");
    scanf("%s", &nombreDisco);
    char ruta[60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, nombreDisco);
    strcat(ruta, ".vd");
    FILE *archivo = fopen(ruta, "rb");
    MBR mbr;

    fread(&mbr, sizeof (MBR), 1, archivo);
    printf("====================================\n");
    printf("Nombre disco: ");
    printf("%s", mbr.nombre);
    printf("\n");
    printf("Tamanio disco : ");
    printf("%d", mbr.tamanio / (1024 * 1024));
    printf(" MB -> ");
    printf("%d", mbr.tamanio / 1024);
    printf(" Kb -> ");
    printf("%d", mbr.tamanio);
    printf(" B");
    printf("\n");
    printf("Cantidad de particiones: ");
    printf("%d", mbr.cantPartiones);
    printf("\n");
    printf("***Particiones***");
    printf("\n");
    for (it = 0; it < 16; it++) {
        if (mbr.particion[it].estado == 1) {
            printf("------------------------------------\n");

            printf("Estado: ");
            printf("%d", mbr.particion[it].estado);
            printf("\n");

            printf("Nombre: ");
            printf("%s", mbr.particion[it].nombre);
            printf("\n");

            printf("Inicio en B: ");
            printf("%d", mbr.particion[it].inicio);
            printf("\n");

            printf("Cantidad bloques: ");
            printf("%d", mbr.particion[it].cantBloques);
            printf("\n");

            printf("Cantidad bloques libres: ");
            printf("%d", mbr.particion[it].cantBloquesLibres);
            printf("\n");

            printf("Tipo particion: ");
            printf("%d", mbr.particion[it].tipoParticion);
            printf("\n");

            printf("Tipo ajuste: ");
            printf("%d", mbr.particion[it].tipoAjuste);
            printf("\n");

            printf("Tamanio en Kb: ");
            printf("%d", mbr.particion[it].tamanio);
            printf("\n");

            printf("Tamanio bloques en B: ");
            printf("%d", mbr.particion[it].tamanioBloque + sizeof (Bloque));
            printf("\n");

            //ciclos que muestran la fat y los bloques
            printf("++++++++++++++++++++++++++++++++++++\n");
            int j;
            //imprime la fat
            fseek(archivo, mbr.particion[it].inicio, SEEK_SET);

            for (j = 0; j < mbr.particion[it].cantBloques; j++) {
                if ((mbr.particion[it].tipoParticion == 1 || mbr.particion[it].tipoParticion == 3) && mbr.particion[it].estado == 1) {
                    char fat;
                    fread(&fat, 1, 1, archivo);
                    printf("%c", fat);
                    printf(" ");
                }
            }
            //si se lee la fat, comentar la instruccion siguiente
            //fseek(archivo,mbr.particion[it].cantBloques,SEEK_CUR);



            fseek(archivo, sizeof (Root)*256, SEEK_CUR);
            printf("\n");
            char cadenaCompleta[10000]="";
            for (j = 0; j < mbr.particion[it].cantBloques; j++) {
                if ((mbr.particion[it].tipoParticion == 1 || mbr.particion[it].tipoParticion == 3) && mbr.particion[it].estado == 1) {
                    //fseek(archivo,sizeof(Bloque),SEEK_CUR);
                    Bloque b;
                    fread(&b, sizeof (Bloque), 1, archivo);
                    char *dato = (char *) malloc(mbr.particion[it].tamanioBloque);
                    //char dato[mbr.particion[it].tamanioBloque];
                    fread(dato, mbr.particion[it].tamanioBloque, 1, archivo);
                    if(b.estado=='1'){
                    strcat(cadenaCompleta,dato);
                    }
                    printf("%s", dato);
                    // printf("%d", j); //concatena un numero diferente a cada bloque para diferenciarlos
                    //printf(" sig ");
                    //printf("%d",b.siguiente);
                    printf(" ");
                }

            }
            printf("\n++++++++++++++++++++++++++++++++++++\n\n\n\n");

            printf("%s",cadenaCompleta);
            printf("\n------------------------------------\n");
        }
    }

    printf("====================================\n");


    fflush(archivo);
    fclose(archivo);
}

void reporteDeArchivosDisco() {
    FILE *archivoIndex = fopen(rutaGeneral, "rb");
    IndexDisco index;

    char ruta[60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/reporteDeArchivosDisco.csv");
    FILE *reporte1 = fopen(ruta, "w");

    fprintf(reporte1, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n", "ID", ",", "Nombre", ",", "Tamaño", ",", "Primarias", ",", "Extendidas", ",", "Logicas", ",", "SinParticionar", ",", "Estado");
    char estado[32];
    while (fread(&index, sizeof (IndexDisco), 1, archivoIndex)) {
        if (index.estado == 0) {
            strcpy(estado, "Eliminado");
        } else {
            strcpy(estado, "Existe");
        }
        fprintf(reporte1, "%s %s %s %s %d %s %d %s %d %s %d %s %d %s %s\n", index.id, ",", index.nombre, ",", index.tamanio / (1024 * 1024), ",", index.particionesPrimarias, ",", index.particionesExtendidas, ",", index.particionesLogicas, ",", index.espacioSinParticionar / (1024 * 1024), ",", estado);
    }

    fflush(reporte1);
    fclose(reporte1);

    fflush(archivoIndex);
    fclose(archivoIndex);

    char rutaCsv[1000];
    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, ruta);
    system(rutaCsv);

}

void reporteMBR(){

    printf("Id del disco: ");
    char id[32];
    scanf("%s", &id);

    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }

    char ruta[60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    MBR mbr;
    FILE *archivo = fopen(ruta, "rb");
    fread(&mbr, sizeof (MBR), 1, archivo);


    char rutaMBR[60];//ruta para crear el archivo .dot
    strcpy(rutaMBR, rutaCarpeta);
    strcat(rutaMBR, "/ReporteMBR.dot");
    FILE *reporteMBR = fopen(rutaMBR, "w");

    char cadenaDot[10000] = "";//cadena donde se guarda las instrucciones de graphiz
    char encabezado[1000];
    strcpy(encabezado,"\"");
    strcat(encabezado,mbr.nombre);
    strcat(encabezado,", ");
    char aux[30];
    sprintf(aux,"%d",mbr.tamanio/1024*1024);
    strcat(encabezado,aux);
    strcat(encabezado," MB, ");
    sprintf(aux,"%d",mbr.cantPartiones);
    strcat(encabezado,aux);
    strcat(encabezado," Particion/es\"");
    strcat(cadenaDot, "digraph structs { subgraph cluster0 {label=");
    strcat(cadenaDot,encabezado);
    strcat(cadenaDot," struct34 [shape=record,label=\"{");
    char *particionDot=(char *)malloc(mbr.cantPartiones*1000);
    strcpy(particionDot,"");
    if(mbr.cantPartiones==0){
       strcpy(particionDot,"No hay particiones");
    }

    char estado[32];
    char tipoP [32];
    char tipoA [32];
    char tipoF [32];
    int i;
    for (i = 0; i < mbr.cantPartiones; i++) {
        if(mbr.particion[i].estado==1){

        switch (mbr.particion[i].tipoParticion) {
        case 1:
            strcpy(tipoP,"Primaria");
            break;
        case 2:
            strcpy(tipoP,"Extendida");
            break;
        case 3:
            strcpy(tipoP,"Logica");
            break;
        }

        switch (mbr.particion[i].tipoAjuste) {
        case 1:
            strcpy(tipoA,"Primer ajuste");
            break;
        case 2:
            strcpy(tipoA,"Mejor ajuste");
            break;
        case 3:
            strcpy(tipoA,"Peor ajuste");
            break;
        }

        switch (mbr.particion[i].estado) {
        case 0:
            strcpy(estado,"Eliminada");
            break;
        case 1:
            strcpy(estado,"Activa");
            break;
        }

        switch (mbr.particion[i].tipoFormato) {
        case 1:
            strcpy(tipoF,"Fat");
            break;
        case 2:
            strcpy(tipoF,"Enlazado");
            break;
        case 3:
            strcpy(tipoF,"Ext3");
            break;
        }

        if(i==mbr.cantPartiones-1){
            if(mbr.particion[i].tipoParticion==2){
                strcat(particionDot,estado);
                strcat(particionDot,", ");
                strcat(particionDot,mbr.particion[i].nombre);
                strcat(particionDot,", Inicia en ");
                sprintf(aux,"%d",mbr.particion[i].inicio);
                strcat(particionDot,aux);
                strcat(particionDot,", ");
                strcat(particionDot,tipoP);
                strcat(particionDot,", ");
                strcat(particionDot,tipoA);
                strcat(particionDot,", ");
                sprintf(aux,"%d",mbr.particion[i].tamanio/1024);
                strcat(particionDot,aux);
                strcat(particionDot," MB  (");
                sprintf(aux,"%d",mbr.particion[i].tamanio);
                strcat(particionDot,aux);
                strcat(particionDot," Kb) ");
            }else{
                strcat(particionDot,estado);
                strcat(particionDot,", ");
                strcat(particionDot,mbr.particion[i].nombre);
                strcat(particionDot,", Inicia en ");
                sprintf(aux,"%d",mbr.particion[i].inicio);
                strcat(particionDot,aux);
                strcat(particionDot,", ");
                sprintf(aux,"%d",mbr.particion[i].cantBloques);
                strcat(particionDot,aux);
                strcat(particionDot," Bloque/s, ");
                sprintf(aux,"%d",mbr.particion[i].cantBloquesLibres);
                strcat(particionDot,aux);
                strcat(particionDot," Bloque/s libre/s, ");
                strcat(particionDot,tipoP);
                strcat(particionDot,", ");
                strcat(particionDot,tipoA);
                strcat(particionDot,", ");
                strcat(particionDot,tipoF);
                strcat(particionDot,", ");
                sprintf(aux,"%d",mbr.particion[i].tamanio/1024);
                strcat(particionDot,aux);
                strcat(particionDot," MB  (");
                sprintf(aux,"%d",mbr.particion[i].tamanio);
                strcat(particionDot,aux);
                strcat(particionDot," Kb) ");
            }
        }else{

            if(mbr.particion[i].tipoParticion==2){
                strcat(particionDot,estado);
                strcat(particionDot,", ");
                strcat(particionDot,mbr.particion[i].nombre);
                strcat(particionDot,", Inicia en ");
                sprintf(aux,"%d",mbr.particion[i].inicio);
                strcat(particionDot,aux);
                strcat(particionDot,", ");
                strcat(particionDot,tipoP);
                strcat(particionDot,", ");
                strcat(particionDot,tipoA);
                strcat(particionDot,", ");
                sprintf(aux,"%d",mbr.particion[i].tamanio/1024);
                strcat(particionDot,aux);
                strcat(particionDot," MB  (");
                sprintf(aux,"%d",mbr.particion[i].tamanio);
                strcat(particionDot,aux);
                strcat(particionDot," Kb) |");
            }else{
                strcat(particionDot,estado);
                strcat(particionDot,", ");
                strcat(particionDot,mbr.particion[i].nombre);
                strcat(particionDot,", Inicia en ");
                sprintf(aux,"%d",mbr.particion[i].inicio);
                strcat(particionDot,aux);
                strcat(particionDot,", ");
                sprintf(aux,"%d",mbr.particion[i].cantBloques);
                strcat(particionDot,aux);
                strcat(particionDot," Bloque/s, ");
                sprintf(aux,"%d",mbr.particion[i].cantBloquesLibres);
                strcat(particionDot,aux);
                strcat(particionDot," Bloque/s libre/s, ");
                strcat(particionDot,tipoP);
                strcat(particionDot,", ");
                strcat(particionDot,tipoA);
                strcat(particionDot,", ");
                strcat(particionDot,tipoF);
                strcat(particionDot,", ");
                sprintf(aux,"%d",mbr.particion[i].tamanio/1024);
                strcat(particionDot,aux);
                strcat(particionDot," MB  (");
                sprintf(aux,"%d",mbr.particion[i].tamanio);
                strcat(particionDot,aux);
                strcat(particionDot," Kb) |");
            }
        }
    }
    }
    strcat(cadenaDot,particionDot);
    strcat(cadenaDot,"}\"];}}");
    fprintf(reporteMBR, "%s", cadenaDot);
    printf("\n");
    fflush(reporteMBR);
    fclose(reporteMBR);
    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);

    //instrucciones para crear la imagen png desde el archivo .dot
    char rutaDot [1000];
    strcpy(rutaDot, "dot -Tpng ");
    strcat(rutaDot, rutaMBR);
    strcat(rutaDot, " -o ");
    strcat(rutaDot, rutaCarpeta);
    strcat(rutaDot, "/ReporteMBR.png");
    system(rutaDot);

    //abre la imagen con el editor de imagenes shotwell
    char rutaPNG [1000];
    strcpy(rutaPNG, "shotwell ");
    strcat(rutaPNG, rutaCarpeta);
    strcat(rutaPNG, "/ReporteMBR.png");
    system(rutaPNG);

}

void reporteDeParticion() {
    printf("Id del disco: ");
    char id[32];
    scanf("%s", &id);

    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }

    printf("Nombre de la particion: ");
    char nombreP[32];
    scanf("%s", &nombreP);
    int op = existeParticion13(nombreP, indexDisco.nombre);
    if (op == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (op == -1) {
        printf("-----Una particion extendida no posee FAT,Root y bloques-----\n\n");
        return;
    }

    char ruta[60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    MBR mbr;
    FILE *archivo = fopen(ruta, "rb");
    fread(&mbr, sizeof (MBR), 1, archivo);

    for (it = 0; it < 16; it++) {
        if (!strcmp(nombreP, mbr.particion[it].nombre)) {
            break;
        }
    }

    fseek(archivo, mbr.particion[it].inicio, SEEK_SET);

    char rutaFat[60];
    strcpy(rutaFat, rutaCarpeta);
    strcat(rutaFat, "/Fat.csv");
    FILE *reporteFat = fopen(rutaFat, "w");

    int i;
    int cont = 0;
    for (i = 0; i < mbr.particion[it].cantBloques; i++) {
        char c;
        fread(&c, 1, 1, archivo);
        fprintf(reporteFat, "%c %s", c, ",");
        cont += 1;
        if (cont == 69) {
            cont = 0;
            fprintf(reporteFat, "\n");
        }

    }

    fseek(archivo,mbr.particion[it].inicio+mbr.particion[it].cantBloques,SEEK_SET);

    char rutaRoot[60];
    strcpy(rutaRoot, rutaCarpeta);
    strcat(rutaRoot, "/rutaRoot.csv");
    FILE *reporteRoot = fopen(rutaRoot, "w");
    fprintf(reporteRoot,"%s %s %s %s %s %s %s","Nombre,","Extension,","Creacion,","Tamaño,","Inicio,","Final,","Estado\n");
    int ro;
    for(ro=0;ro<256;ro++){
        Root root;
        fread(&root,sizeof(Root),1,archivo);
        if(root.tamanio>0){
            fprintf(reporteRoot,"%s %s %s %s %s %s %d %s %d %s %d %s %d %s",root.nombre,",",root.extension,",",root.fechaCreacion,",",root.tamanio,",",root.iDInicio,",",root.iDFinal,",",root.estado,"\n");
        }
    }



    char rutaBloque[60];
    strcpy(rutaBloque, rutaCarpeta);
    strcat(rutaBloque, "/reporteBloque.csv");
    FILE *reporteBloque = fopen(rutaBloque, "w");

    fprintf(reporteBloque,"%s %s %s %s %s","Id,","Estado,","Anterior,","Siguiente,","Datos\n");
    fseek(archivo,mbr.particion[it].inicio+mbr.particion[it].cantBloques+256*(sizeof(Root)),SEEK_SET);
    for(ro=0;ro<mbr.particion[it].cantBloques;ro++){
        Bloque bloque;
        fread(&bloque,sizeof(Bloque),1,archivo);
        char *dato=(char *)malloc(mbr.particion[it].tamanioBloque);
        fread(dato,mbr.particion[it].tamanioBloque,1,archivo);
        fprintf(reporteBloque,"%d %s %c %s %d %s %d %s %s %s",bloque.iD,",",bloque.estado,",",bloque.anterior,",",bloque.siguiente,",",dato,"\n");
    }

    printf("\n\n");
    fflush(reporteBloque);
    fclose(reporteBloque);
    fflush(reporteFat);
    fclose(reporteFat);
    fflush(reporteRoot);
    fclose(reporteRoot);
    fflush(archivo);
    fclose(archivo);
    fflush(archivoIndex);
    fclose(archivoIndex);

    char rutaCsv[1000];
    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, rutaFat);
    system(rutaCsv);

    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, rutaRoot);
    system(rutaCsv);

    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, rutaBloque);
    system(rutaCsv);

}

void reporteDeParticionesDelDisco() {
    printf("Id del disco: ");
    char id[32];
    scanf("%s", &id);

    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }

    char ruta[60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    MBR mbr;
    FILE *archivo = fopen(ruta, "rb");
    fread(&mbr, sizeof (MBR), 1, archivo);

    char rutaD[60];//ruta para crear el archivo .dot
    strcpy(rutaD, rutaCarpeta);
    strcat(rutaD, "/Disco.dot");
    FILE *reporteD = fopen(rutaD, "w");

    int i;
    char cadenaDot[10000] = "";//cadena donde se guarda las instrucciones de graphiz
    strcat(cadenaDot, "digraph structs { subgraph cluster0 { struct34 [shape=record,label=\"MBR");

    int cantLo = 0;
    for (i = 0; i < 16; i++) {
        if (mbr.particion[i].estado == 1) {

            if (mbr.particion[i].tipoParticion == 1) {

                strcat(cadenaDot, "|Primaria");
            } else if (mbr.particion[i].tipoParticion == 2) {


                strcat(cadenaDot, "|{Extendida|{");
                if (indexDisco.particionesLogicas > 1) {
                    int j;
                    for (j = 0; j < 16; j++) {
                        if (mbr.particion[j].tipoParticion == 3 && mbr.particion[j].estado == 1) {
                            if (cantLo == 0) {
                                strcat(cadenaDot, "Logica");
                                cantLo += 1;
                            } else {
                                cantLo += 1;

                                if (cantLo == indexDisco.particionesLogicas) {
                                    strcat(cadenaDot, "|Logica}}");

                                } else {
                                    strcat(cadenaDot, "|Logica");
                                }
                            }
                        }
                    }

                } else if (indexDisco.particionesLogicas == 1) {
                    strcat(cadenaDot, "logica}}");
                } else {
                    strcat(cadenaDot, "}}");
                }
            }

        }
    }





    if (indexDisco.espacioSinParticionar > 0) {
        strcat(cadenaDot, "|Libre");
    }
    strcat(cadenaDot, "\"];}}");
    fprintf(reporteD, "%s", cadenaDot);
    printf("\n");
    fflush(reporteD);
    fclose(reporteD);

    fflush(archivo);
    fclose(archivo);

    fflush(archivoIndex);
    fclose(archivoIndex);

    //instrucciones para crear la imagen png desde el archivo .dot
    char rutaDot [1000];
    strcpy(rutaDot, "dot -Tpng ");
    strcat(rutaDot, rutaD);
    strcat(rutaDot, " -o ");
    strcat(rutaDot, rutaCarpeta);
    strcat(rutaDot, "/Disco.png");
    system(rutaDot);

    //abre la imagen con el editor de imagenes shotwell
    char rutaPNG [1000];
    strcpy(rutaPNG, "shotwell ");
    strcat(rutaPNG, rutaCarpeta);
    strcat(rutaPNG, "/Disco.png");
    system(rutaPNG);

}




//Fase2

//SISTEMA DE ARCHIVOS ENLAZADO
void sistemaEnlazado(FILE **archivoDisco,Particion *particion,int cantBloques){

    ArchivoEnlazado archivo;
    archivo.fin=-1;
    archivo.inicio=-1;
    fseek(archivoDisco,particion->inicio,SEEK_SET);

    fwrite(&archivo,sizeof(ArchivoEnlazado),1,archivoDisco);
    int i;
    for(i=0;i<cantBloques;i++){
        BloqueEnlazado bloque;
        char *dato=(char *)malloc(100);
        strcpy(bloque.contenido,dato);
        bloque.apuntador=-2;
        fwrite(&bloque,sizeof(BloqueEnlazado),1,archivoDisco);
    }
}

void cargarCSV(){

    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=2){
        printf("\n---La particion no posee un sistema de archivos enlazado---\n\n");
        return;
    }


    printf("Ingrese la ruta del archivo csv");
    char rutaCSV[60];
    scanf("%s",&rutaCSV);
    printf("Ingrese el nombre del archivo csv");
    char nombreCSV[32];
    scanf("%s",&nombreCSV);
    FILE *archivoCSV = fopen(rutaCSV, "rb+");
    fseek(archivoCSV,0L, SEEK_END);
    int tamanio=ftell(archivoCSV);
    fseek(archivoCSV,0, SEEK_SET);
    char contenido[100];
    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado),SEEK_SET);
    ArchivoEnlazado archivoEnlazado;
    //stpcpy(archivoEnlazado.nombre,"archivo1");
    //archivoEnlazado.fin=0;
    //archivoEnlazado.fin=100;
    //fwrite(&archivoEnlazado,sizeof(archivoEnlazado),1,archivo);
    int apun=0;
    int cantBlo=ceil(((float)tamanio/((float)100)));
    int j;
    for(j=0;j<cantBlo;j++) {
        if(j==cantBlo-1 & tamanio<100){
            char contenidoFinal[100];
            fread(&contenidoFinal,tamanio, 1, archivoCSV);
            BloqueEnlazado bloque;
            bloque.apuntador=-1;
            strcpy(bloque.contenido,contenidoFinal);
            int it;
            for(it=tamanio;it<100;it++){
               bloque.contenido[it]='\0';
            }

            fwrite(&bloque,sizeof(BloqueEnlazado),1,archivo);

        }else{
        fread(&contenido, 100, 1, archivoCSV);
        BloqueEnlazado bloque;
        bloque.apuntador=apun+1;
        strcpy(bloque.contenido,contenido);
        fwrite(&bloque,sizeof(BloqueEnlazado),1,archivo);
        tamanio=tamanio-100;
        }
        apun++;
    }

    stpcpy(archivoEnlazado.nombre,nombreCSV);
    archivoEnlazado.inicio=0;
    archivoEnlazado.fin=apun;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fwrite(&archivoEnlazado,sizeof(archivoEnlazado),1,archivo);
    fflush(archivoCSV);
    fclose(archivoCSV);
    fflush(archivo);
    fclose(archivo);
    fclose(archivoIndex);
}

void leerArchivoCSV(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----En una particion extendida no existen archivos-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }


    if(mbr.particion[z].tipoFormato!=2){
        printf("\n---La particion no posee un sistema de archivos enlazado---\n\n");
        return;
    }

    printf("Ingrese el nombre del archivo csv");
    char nombreArchivo[32];
    scanf("%s",&nombreArchivo);
    ArchivoEnlazado archivoEnlazado;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&archivoEnlazado, sizeof (archivoEnlazado), 1, archivo);
    if(strcmp(nombreArchivo,archivoEnlazado.nombre)){
        printf("No existe el archivo\n\n");
        //return;
    }

    char rutaCSV[60];
    strcpy(rutaCSV,rutaCarpeta);
    strcat(rutaCSV,"/temporalCSV.csv");

    FILE *archivoCSV = fopen(rutaCSV, "wb+");
    char contenido[100];
    int i;
    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado),SEEK_SET);
    for(i=archivoEnlazado.inicio;i<archivoEnlazado.fin;i++){
        fseek(archivo,4,SEEK_CUR);
        fread(&contenido, 100, 1, archivo);
        fwrite(&contenido, 100, 1, archivoCSV);

    }

    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);
    fflush(archivoCSV);
    fclose(archivoCSV);
    char rutaCsv[1000];
    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, rutaCSV);
    system(rutaCsv);
    remove(rutaCSV);
}


void mostrarBloquesCSV(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----En una particion extendida no existen archivos-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }


    if(mbr.particion[z].tipoFormato!=2){
        printf("\n---La particion no posee un sistema de archivos enlazado---\n\n");
        return;
    }

    printf("Ingrese el nombre del archivo csv: ");
    char nombreArchivo[32];
    scanf("%s",&nombreArchivo);
    ArchivoEnlazado archivoEnlazado;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&archivoEnlazado, sizeof (archivoEnlazado), 1, archivo);
    int cmp=strcmp(nombreArchivo,archivoEnlazado.nombre);
    if(cmp==-1){
        printf("No existe el archivo\n\n");
        return;
    }

    char rutaCSV[60];
    strcpy(rutaCSV,rutaCarpeta);
    strcat(rutaCSV,"/temporalBloqueCSV.doc");

    FILE *archivoCSV = fopen(rutaCSV, "wb+");

    char bloqueBorrado[32];
    strcpy(bloqueBorrado,"Bloque Eliminado");
    int i;
    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado),SEEK_SET);
    BloqueEnlazado bloque;
    for(i=archivoEnlazado.inicio;i<archivoEnlazado.fin;i++){

        fread(&bloque, sizeof(BloqueEnlazado), 1, archivo);
        char detalle[50];
        strcpy(detalle,"\n\nBloque ");
        char aux[5];
        sprintf(aux,"%d",i);
        strcat(detalle,aux);
        strcat(detalle," :\n");
        fwrite(&detalle,strlen(detalle),1,archivoCSV);
        int tam=strlen(bloque.contenido);
        if(bloque.apuntador!=-2){
        if(tam<100){
            fwrite(&bloque.contenido, tam, 1, archivoCSV);
        }else{
            fwrite(&bloque.contenido, 100, 1, archivoCSV);
        }
        }else{

            fwrite(&bloqueBorrado, strlen(bloqueBorrado), 1, archivoCSV);
        }

    }

    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);
    fflush(archivoCSV);
    fclose(archivoCSV);
    char rutaCsv[1000];
    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, rutaCSV);
    system(rutaCsv);
    remove(rutaCSV);
}





void borrarBloqueCSV(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----En una particion extendida no existen archivos-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=2){
        printf("\n---La particion no posee un sistema de archivos enlazado---\n\n");
        return;
    }


    printf("Ingrese el nombre del archivo csv: ");
    char nombreArchivo[32];
    scanf("%s",&nombreArchivo);
    ArchivoEnlazado archivoEnlazado;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&archivoEnlazado, sizeof (archivoEnlazado), 1, archivo);
    int cmp=strcmp(nombreArchivo,archivoEnlazado.nombre);
    if(cmp!=0){
        printf("No existe el archivo\n\n");
        return;
    }

    printf("Ingrese id del bloque a eliminar: ");
    int idBloque;
    scanf("%d",&idBloque);

    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado)+sizeof(BloqueEnlazado)*idBloque,SEEK_SET);
    BloqueEnlazado bloque;
    fread(&bloque,sizeof(BloqueEnlazado),1,archivo);

    if(bloque.apuntador==-2||(idBloque<0||idBloque>(archivoEnlazado.fin-1))){
        printf("\nEl bloque no existe\n\n");
        return;
    }



    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado)+sizeof(BloqueEnlazado)*idBloque,SEEK_SET);
    char *dato=(char *)malloc(100);
    strcpy(bloque.contenido,dato);
    bloque.apuntador=-2;
    fwrite(&bloque,sizeof(BloqueEnlazado),1,archivo);
    printf("\n---Archivo eliminado---\n\n");

    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);
\

}




void agregarBloqueCSV(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----En una particion extendida no existen archivos-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }


    if(mbr.particion[z].tipoFormato!=2){
        printf("\n---La particion no posee un sistema de archivos enlazado---\n\n");
        return;
    }

    printf("Ingrese el nombre del archivo csv: ");
    char nombreArchivo[32];
    scanf("%s",&nombreArchivo);
    ArchivoEnlazado archivoEnlazado;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&archivoEnlazado, sizeof (archivoEnlazado), 1, archivo);
    int cmp=strcmp(nombreArchivo,archivoEnlazado.nombre);
    if(cmp!=0){
        printf("No existe el archivo\n\n");
        return;
    }


    int cantBloques=(mbr.particion[z].tamanio*1024-sizeof(ArchivoEnlazado))/100;

    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado),SEEK_SET);
    int i;
    BloqueEnlazado bloque;
    for(i=0;i< cantBloques;i++){
    fread(&bloque,sizeof(BloqueEnlazado),1,archivo);
        if(bloque.apuntador==-2){
            break;
        }

        if(i==(cantBloques-1)){
            printf("\nNo hay espacio disponible---\n\n");
            return;
        }

    }


    //texto a ingresar

    char c[10000]="";
    char texto[100000]="";
    printf("Ingrese el comando echo:term en una linea nueva para terminar de escribir el bloque\n");
    printf("Ingrese el texto del bloque:\nMaxico 100 caracteres\n");

    do{

        scanf(" %[^\n]",&c);


        if(!strcmp(c,"echo:term")){
            break;
        }else{
            strcat(texto,c);
            strcat(texto,"\n");
        }
    }while(1);


    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado)+sizeof(BloqueEnlazado)*i,SEEK_SET);
    //char *dato=(char *)malloc(100);
    strcpy(bloque.contenido,texto);
    bloque.apuntador=i;
    fwrite(&bloque,sizeof(BloqueEnlazado),1,archivo);
    printf("\n---Bloque agregado---\n\n");

    if(i>=archivoEnlazado.fin){
        archivoEnlazado.fin=i+1;
    }
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fwrite(&archivoEnlazado,sizeof(ArchivoEnlazado),1,archivo);
    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);
\

}


//SISTEMA DE ARCHIVOS EXT3
void sistemaExt3(FILE **archivoDisco,Particion *particion,int cantBloques){



    time_t tiempo = time(0);
    int inicio=particion->inicio;
    SuperBloque superBloque;
    superBloque.apunBitacora=inicio+sizeof(SuperBloque)+2*cantBloques+cantBloques*sizeof(Inodo)+cantBloques*sizeof(BloqueExt3);
    superBloque.apunBitMatFicheros=inicio+sizeof(SuperBloque);
    superBloque.apunBloque=inicio+sizeof(SuperBloque)+2*cantBloques+cantBloques*sizeof(Inodo);
    superBloque.apunInodo=inicio+sizeof(SuperBloque)+2*cantBloques;
    superBloque.bloquesLibres=cantBloques;
    superBloque.contadorMontados=superBloque.contadorMontados+1;
    //superBloque.fechaDesmontaje;
    superBloque.fechaMontaje=localtime(&tiempo);
    superBloque.firstFreeBitMapBloque=0;
    superBloque.firstFreeBitMapInodo=0;
    superBloque.firstFreeBloque=0;
    superBloque.firstFreeInodo=0;
    superBloque.inodosLibres=cantBloques;
    superBloque.numeroBloques=cantBloques;
    superBloque.numeroInodos=cantBloques;
    superBloque.numeroMagico=201213402;
    superBloque.tamanioBloque=100;

    fseek(archivoDisco,particion->inicio,SEEK_SET);
    fwrite(&superBloque,sizeof(SuperBloque),1,archivoDisco);
    int bit;
    //agrega el bitmap para inodos
    for(bit=0;bit<cantBloques;bit++){
        char c='0';
        fwrite(&c,1,1,archivoDisco);
    }
    //abrega el bitmap para bloques
    for(bit=0;bit<cantBloques;bit++){
        char c='0';
        fwrite(&c,1,1,archivoDisco);
    }

    //se crea los inodos

    //struct tm *tlocal = localtime(&tiempo);
    for(bit=0;bit<cantBloques;bit++){
        Inodo inodo;
        inodo.cTime=localtime(&tiempo);
        int dir;
        for(dir=0;dir<5;dir++){
            inodo.directo[dir]=-1;
        }
        inodo.indirecto[0]=-1;
        inodo.indirecto[1]=-1;
        inodo.fechaMod=localtime(&tiempo);
        inodo.llave=bit;
        inodo.tamArchivo=-1;
        inodo.timeLastAccess=localtime(&tiempo);
        strcpy(inodo.tipo,"vacio");
        //fseek(archivoDisco,sizeof(SuperBloque)+superBloque.numeroBloques,SEEK_SET);
        fwrite(&inodo,sizeof(Inodo),1,archivoDisco);
    }
    //Inodo inodo;
    //fseek(archivoDisco,sizeof(SuperBloque)+2*cantBloques,SEEK_SET);
    //fread(&inodo,sizeof(Inodo),1,archivoDisco);

    for(bit=0;bit<cantBloques;bit++){
        BloqueExt3 bloque;

        int apun;
        for(apun=0;apun<6;apun++){
            bloque.apuntadores[apun]=-1;
        }
        char *conte=(char *)malloc(64);
        strcpy(bloque.contenido,conte);
        bloque.llave=bit;
        strcpy(bloque.nombre,"");
        strcpy(bloque.padre,"");
        //fseek(archivoDisco,sizeof(SuperBloque)+2*superBloque.numeroBloques,SEEK_SET);
        fwrite(&bloque,sizeof(BloqueExt3),1,archivoDisco);
    }

    int contLog;
    for(contLog=0;contLog<150;contLog++){
        Log log;
        char *contLog=(char *)malloc(100);
        strcpy(log.contenido,contLog);
        log.tipo=-1;
        fwrite(&log,sizeof(Log),1,archivoDisco);

    }

}

void crearDirectorio(){

    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }



    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    printf("Ingrese la ruda del directorio: ");
    char rutaDir[100];
    scanf("%s", &rutaDir);
    printf("Nombre del directorio: ");
    char nombreDir[50];
    scanf("%s", &nombreDir);
    char path[200];
    strcpy(path,rutaDir);
    strcat(path,"/");
    strcat(path,nombreDir);


    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);
    time_t tiempo = time(0);
    char bitInodo;
    fread(&bitInodo, 1, 1, archivo);
    printf("%c",bitInodo);


    //char string[] = "/home";
    char *token;
    char rutaDir2[100];
    strcpy(rutaDir2,rutaDir);
    char rutaDir3[100];
    strcpy(rutaDir3,rutaDir);
    token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
    if(esRaiz(rutaDir2)==0){
        if(bitInodo=='0'){
            fseek(archivo,superBloque.apunInodo,SEEK_SET);
            //while (token != NULL){
            int tams=sizeof(SuperBloque);
                Inodo inodo;
                fread(&inodo,sizeof(Inodo), 1, archivo);
                inodo.directo[0]=superBloque.firstFreeBloque;
                strcpy(inodo.tipo,"Carpeta");
                inodo.fechaMod=localtime(&tiempo);
                fseek(archivo,superBloque.apunInodo,SEEK_SET);
                fwrite(&inodo,sizeof(Inodo), 1, archivo);

                fseek(archivo,superBloque.apunBloque,SEEK_SET);
                BloqueExt3 bloque;
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                strcpy(bloque.nombre,nombreDir);
                strcpy(bloque.padre,"null");
                fseek(archivo,superBloque.apunBloque,SEEK_SET);
                fwrite(&bloque,sizeof(BloqueExt3),1,archivo);

                Log bitacora;
                strcpy(bitacora.nombre,path);
                strcpy(bitacora.tipoOperacion,"Creacion de carpeta");
                bitacora.tipo=1;
                bitacora.fecha=localtime(&tiempo);
                fseek(archivo,superBloque.apunBitacora,SEEK_SET);
                fwrite(&bitacora,sizeof(Log), 1, archivo);

                modificarSuperBloque(archivo,&superBloque,&mbr.particion[z],1);
                superBloque.apunBitacora=superBloque.apunBitacora+sizeof(Log);
                fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
                fwrite(&superBloque,sizeof(SuperBloque), 1, archivo);
                //token = strtok(NULL,"/");
           // }
        }else{
        printf("\n---Ya existe un directorio raiz---\n\n");
        }
    }else{

    Inodo inodo;
    BloqueExt3 bloque;
    int posicionInodo=superBloque.apunInodo;
    int posicionBloque;


    //printf("rudaDir: %s\n",rutaDir3);
    token = strtok(rutaDir3 , "/");
    //return;
    int profundidad=0;
    while (token != NULL){

        if(profundidad==0){
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);
            int tipo=strcmp(inodo.tipo,"Carpeta");
            if(tipo==0){
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                    int cmp=strcmp(token,bloque.nombre);
                    if(cmp==0){
                        //posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                    }else{
                        printf("\n---La ruta no existe---\n\n");
                        return;
                    }
            }
        }else{
            int i;
            for(i=0;i<6;i++){
                posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");
                if(tipo==0){
                    BloqueExt3 aux=bloque;
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                   if(strcmp(bloque.nombre,token)==0){
                       break;
                   }
                   if(i==5){
                       printf("\n---La ruta no existe---\n\n");
                       break;
                   }
                   bloque=aux;
                }
                if(i==5){
                    printf("\n---La ruta no existe---\n\n");
                    break;
                }
            }

        }

        profundidad++;
        token = strtok(NULL,"/");
    }

    //verifica si existe una carpeta con el mismo nombre en el directorio
    int rep;
    int posicionActualBloque=posicionBloque;
    for(rep=0;rep<6;rep++){
        posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
        fseek(archivo,posicionInodo,SEEK_SET);
        fread(&inodo,sizeof(Inodo), 1, archivo);
        int tipo=strcmp(inodo.tipo,"Carpeta");
        if(tipo==0){
            BloqueExt3 aux=bloque;
            posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
           if(strcmp(bloque.nombre,nombreDir)==0){
               printf("\n---Ya existe una carpeta con el mismo nombre en esta ruta---\n\n");
               return;
           }
           bloque=aux;
        }
    }


    int hijo=obtenerHijo(bloque.apuntadores);
    if(hijo==-2){
        printf("\n---No se pueden crear mas carpetas en este directorio---\n\n");
        return;
    }else{
        fseek(archivo,posicionActualBloque,SEEK_SET);
        bloque.apuntadores[hijo]=superBloque.firstFreeBitMapInodo;
        fwrite(&bloque,sizeof(BloqueExt3), 1, archivo);
    }

    Inodo inodoNew;
    fseek(archivo,superBloque.apunInodo+superBloque.firstFreeBitMapInodo*sizeof(Inodo),SEEK_SET);
    fread(&inodoNew,sizeof(Inodo), 1, archivo);
    int directo=obtenerDirecto(inodoNew.directo);
    inodoNew.directo[directo]=superBloque.firstFreeBloque;
    strcpy(inodoNew.tipo,"Carpeta");
    inodoNew.fechaMod=localtime(&tiempo);
    fseek(archivo,superBloque.apunInodo+superBloque.firstFreeBitMapInodo*sizeof(Inodo),SEEK_SET);
    fwrite(&inodoNew,sizeof(Inodo), 1, archivo);

    fseek(archivo,superBloque.apunBloque+superBloque.firstFreeBitMapBloque*sizeof(BloqueExt3),SEEK_SET);
    BloqueExt3 bloqueNew;
    fread(&bloqueNew,sizeof(BloqueExt3), 1, archivo);
    strcpy(bloqueNew.nombre,nombreDir);
    strcpy(bloqueNew.padre,bloque.nombre);
    fseek(archivo,superBloque.apunBloque+superBloque.firstFreeBitMapBloque*sizeof(BloqueExt3),SEEK_SET);
    fwrite(&bloqueNew,sizeof(BloqueExt3),1,archivo);


    Log bitacora;
    strcpy(bitacora.nombre,path);
    strcpy(bitacora.tipoOperacion,"Creacion de carpeta");
    bitacora.tipo=1;
    bitacora.fecha=localtime(&tiempo);
    fseek(archivo,superBloque.apunBitacora,SEEK_SET);
    fwrite(&bitacora,sizeof(Log), 1, archivo);

    modificarSuperBloque(archivo,&superBloque,&mbr.particion[z],1);
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    superBloque.apunBitacora=superBloque.apunBitacora+sizeof(Log);
    fwrite(&superBloque,sizeof(SuperBloque), 1, archivo);




    }

    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);

}

int obtenerDirecto(int directo[]){
    int i;
    int direct=-2;
    for(i=0;i<sizeof(directo);i++){
        if(directo[i]==-1){
            direct=i;
            break;
        }
    }
    return direct;
}

int obtenerHijo(int hijo[]){
    int i;
    int hi=-2;
    for(i=0;i<6;i++){
        if(hijo[i]==-1){
            hi=i;
            break;
        }
    }
    return hi;
}

int esRaiz(char *rutaDir){
    //char string[] = "/home";
    char *token;
    token = strtok(rutaDir , "/"); /*Separamos por diagonal*/
    int cont=0;
    while (token != NULL){
            cont++;
            //printf("El token es: %s\n", token);
            token = strtok(NULL,"/");
    }

    return cont;

}

void modificarSuperBloque(FILE **archivo,SuperBloque *superBloque,Particion *particion,int opcion){

   if(opcion==1){
   fseek(archivo,particion->inicio+sizeof(SuperBloque)+superBloque->firstFreeBitMapInodo,SEEK_SET);
   char c='1';
   fwrite(&c,1, 1, archivo);

   fseek(archivo,particion->inicio+sizeof(SuperBloque)+superBloque->numeroBloques+superBloque->firstFreeBitMapBloque,SEEK_SET);
   //char c='1';
   fwrite(&c,1, 1, archivo);
    }
   int i;
   fseek(archivo,superBloque->apunBitMatFicheros,SEEK_SET);
   for(i=0;i<superBloque->numeroBloques;i++){
       char bit;
       fread(&bit,1, 1, archivo);
       if(bit=='0'){
            superBloque->firstFreeBitMapInodo=i;
            break;
        }
    }
   fseek(archivo,superBloque->apunBitMatFicheros+superBloque->numeroInodos,SEEK_SET);
   for(i=0;i<superBloque->numeroBloques;i++){
       char bit;
       fread(&bit,1, 1, archivo);
       if(bit=='0'){
            superBloque->firstFreeBitMapBloque=i;
            break;
        }
    }

   superBloque->firstFreeBloque=superBloque->firstFreeBitMapBloque;
   superBloque->firstFreeInodo=superBloque->firstFreeBitMapInodo;
   superBloque->bloquesLibres=superBloque->bloquesLibres-1;
   superBloque->inodosLibres=superBloque->inodosLibres-1;

   if(opcion==1){
       fseek(archivo,particion->inicio,SEEK_SET);
       fwrite(&superBloque,sizeof(SuperBloque), 1, archivo);
   }

   //fseek(archivo,superBloque->apunBitMatFicheros+superBloque->numeroInodos+5,SEEK_SET);
   //c='1';
   //fwrite(&c,1, 1, archivo);
}

int peorAjusteExt3(FILE **archivo,SuperBloque *superBloque,int cantBloques){
    fseek(archivo,superBloque->apunBitMatFicheros+superBloque->numeroInodos,SEEK_SET);
    int i;
    int inicio=0;
    int contador=0;
    int max=cantBloques;
    int estado=0;
    for(i=0;i<superBloque->numeroBloques;i++){

        char c;
        fread(&c,1,1,archivo);
        if(c=='0'){
            contador+=1;
        }else{
            if(contador>=max){
                max=contador;
                inicio=i-max;
                estado=1;
            }
            contador=0;
        }
        if((i+1)==superBloque->numeroBloques & contador>=max){
            max=contador;
            inicio=(i+1)-max;
            estado=1;
        }

    }
    if(estado==0){
        return -1;
    }else{
       return inicio;
    }


}

void crearArchivoExt3(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }



    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    printf("Ingrese la ruda del directorio: ");
    char rutaDir[100];
    scanf("%s", &rutaDir);
    printf("Nombre del del archivo: ");
    char nombreDir[50];
    scanf("%s", &nombreDir);
    char path [200];
    strcpy(path,rutaDir);
    strcat(path,"/");
    strcat(path,nombreDir);

    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);
    time_t tiempo = time(0);
    char bitInodo;
    fread(&bitInodo, 1, 1, archivo);
    printf("%c",bitInodo);


    //char string[] = "/home";
    char *token;
    char rutaDir2[100];
    strcpy(rutaDir2,rutaDir);
    char rutaDir3[100];
    strcpy(rutaDir3,rutaDir);
    token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
    if(esRaiz(rutaDir2)!=0){

    Inodo inodo;
    BloqueExt3 bloque;
    int posicionInodo=superBloque.apunInodo;
    int posicionBloque;


    //printf("rudaDir: %s\n",rutaDir3);
    token = strtok(rutaDir3 , "/");
    //return;
    int profundidad=0;
    while (token != NULL){

        if(profundidad==0){
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);
            int tipo=strcmp(inodo.tipo,"Carpeta");
            if(tipo==0){
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                    int cmp=strcmp(token,bloque.nombre);
                    if(cmp==0){
                        //posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                    }else{
                        printf("\n---La ruta no existe---\n\n");
                        return;
                    }
            }
        }else{
            int i;
            for(i=0;i<6;i++){
                posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");
                if(tipo==0){
                    BloqueExt3 aux=bloque;
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                   if(strcmp(bloque.nombre,token)==0){
                       break;
                   }
                   if(i==5){
                       printf("\n---La ruta no existe---\n\n");
                       return;
                   }
                   bloque=aux;
                }
            }

        }

        profundidad++;
        token = strtok(NULL,"/");
    }

    //verifica si existe una carpeta con el mismo nombre en el directorio
    int rep;
    int posicionBloqueActual=posicionBloque;
    for(rep=0;rep<6;rep++){
        posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
        fseek(archivo,posicionInodo,SEEK_SET);
        fread(&inodo,sizeof(Inodo), 1, archivo);
        int tipo=strcmp(inodo.tipo,"Archivo");
        if(tipo==0){
            BloqueExt3 aux=bloque;
            posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
           if(strcmp(bloque.nombre,nombreDir)==0){
               printf("\n---Ya existe una archivo con el mismo nombre en esta ruta---\n\n");
               return;
           }
           bloque=aux;
        }
    }


    char rutaTemp[200];
    strcpy(rutaTemp,rutaCarpeta);
    strcat(rutaTemp,"/archivoTemporalExt3.txt");
    textoArchivoExt3(rutaTemp);
    FILE *archivoTemp = fopen(rutaTemp, "rb+");
    fseek(archivoTemp,0L, SEEK_END);
    int tamanio=ftell(archivoTemp);
    fseek(archivoTemp,0, SEEK_SET);


    int hijo=obtenerHijo(bloque.apuntadores);
    if(hijo==-2){
        printf("\n---No se pueden crear mas carpetas en este directorio---\n\n");
    }else{
        fseek(archivo,posicionBloqueActual,SEEK_SET);
        bloque.apuntadores[hijo]=superBloque.firstFreeBitMapInodo;
        fwrite(&bloque,sizeof(BloqueExt3), 1, archivo);
    }
    int cantBloques=ceil(((float)tamanio)/64);
    int inicioArchivo=peorAjusteExt3(archivo,&superBloque,cantBloques);
    fseek(archivo,superBloque.apunBitMatFicheros+superBloque.numeroInodos+inicioArchivo,SEEK_SET);
    int modBit;
    for(modBit=0;modBit<cantBloques;modBit++){
        char c='1';
        fwrite(&c,1,1,archivo);
    }

    fseek(archivo,superBloque.apunBitMatFicheros+superBloque.firstFreeBitMapInodo,SEEK_SET);
    char c='1';
    fwrite(&c,1,1,archivo);

    Inodo inodoNew;
    fseek(archivo,superBloque.apunInodo+superBloque.firstFreeBitMapInodo*sizeof(Inodo),SEEK_SET);
    fread(&inodoNew,sizeof(Inodo), 1, archivo);
    int directo=obtenerDirecto(inodoNew.directo);
    //inodoNew.directo[directo]=superBloque.firstFreeBloque;
    strcpy(inodoNew.tipo,"Archivo");
    inodoNew.fechaMod=localtime(&tiempo);
    inodoNew.tamArchivo=tamanio;
    fseek(archivo,superBloque.apunInodo+superBloque.firstFreeBitMapInodo*sizeof(Inodo),SEEK_SET);

    int ini;
    for(ini=0;ini<cantBloques;ini++){
        inodoNew.directo[ini]=inicioArchivo+ini;
    }

    fwrite(&inodoNew,sizeof(Inodo), 1, archivo);



    int tamArchivRestante=tamanio;
    for(ini=0;ini<cantBloques;ini++){

        char contenidoArchivoExt3[100];


       if(tamArchivRestante<64){
           char auxContenido[tamArchivRestante];
           fread(&auxContenido,tamArchivRestante,1,archivoTemp);
           strcpy(contenidoArchivoExt3,auxContenido);
       }else{
           fread(&contenidoArchivoExt3,64,1,archivoTemp);
       }



        fseek(archivo,superBloque.apunBloque+(inicioArchivo+ini)*sizeof(BloqueExt3),SEEK_SET);
        BloqueExt3 bloqueNew;
        fread(&bloqueNew,sizeof(BloqueExt3), 1, archivo);
        strcpy(bloqueNew.nombre,nombreDir);
        strcpy(bloqueNew.padre,bloque.nombre);
        strcpy(bloqueNew.contenido,contenidoArchivoExt3);

        if(tamArchivRestante<64){
            int i;
            for(i=tamArchivRestante;i<64;i++){
               //bloqueNew.contenido[i]=' ';
            }
            int a;
            a=a+1;
       }

        fseek(archivo,superBloque.apunBloque+(inicioArchivo+ini)*sizeof(BloqueExt3),SEEK_SET);
        fwrite(&bloqueNew,sizeof(BloqueExt3),1,archivo);
        //modificarSuperBloque(archivo,&superBloque,&mbr.particion[z]);
        fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
        fwrite(&superBloque,sizeof(SuperBloque), 1, archivo);
        tamArchivRestante=tamArchivRestante-64;
    }
    //int a=0;
    //printf("%d",a);

    Log bitacora;
    strcpy(bitacora.nombre,path);
    strcpy(bitacora.tipoOperacion,"Creacion de archivo");
    bitacora.tipo=0;
    bitacora.fecha=localtime(&tiempo);
    fseek(archivo,superBloque.apunBitacora,SEEK_SET);
    fwrite(&bitacora,sizeof(Log), 1, archivo);

    modificarSuperBloque(archivo,&superBloque,&mbr.particion[z],0);
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    superBloque.apunBitacora=superBloque.apunBitacora+sizeof(Log);
    fwrite(&superBloque,sizeof(SuperBloque), 1, archivo);
    fflush(archivoTemp);
    fclose(archivoTemp);



    remove("/home/jose/Documentos/Discos/archivoTemporalExt3.txt");

    }

    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);

}

void textoArchivoExt3(char *rutaTemp){
    char c[10000]="";
    char texto[100000]="";
    printf("Ingrese el comando echo:term en una linea nueva para terminar de escribir el archivo\n");
    printf("Ingrese el texto:\n");

    do{

        scanf(" %[^\n]",&c);


        if(!strcmp(c,"echo:term")){
            break;
        }else{
            strcat(texto,c);
            strcat(texto,"\n");
        }
    }while(1);


    FILE *archivo = fopen(rutaTemp, "wb+");
    fwrite(&texto,strlen(texto)-1,1,archivo);
    fflush(archivo);
    fclose(archivo);


}

void leerArchivoExt3(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }




    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    printf("Ingrese la ruda del directorio: ");
    char rutaDir[100];
    scanf("%s", &rutaDir);
   // printf("Nombre del del archivo: ");
    char nombreDir[50]="";
    //scanf("%s", &nombreDir);


    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);
    time_t tiempo = time(0);
    char bitInodo;
    fread(&bitInodo, 1, 1, archivo);
    //printf("%c",bitInodo);

    char *tokenDir;
    char *tokenNombre;
    char entradaRuta1[200];
    char entradaRuta2[200];
    strcpy(entradaRuta1,rutaDir);
    strcpy(entradaRuta2,rutaDir);
    char rutaCarpeta[300];
    tokenDir=strtok(entradaRuta1,"/");
    obtenerRutaCarpeta(tokenDir,rutaCarpeta);
    tokenNombre=strtok(entradaRuta2,"/");
    obtenerNombre(tokenNombre,nombreDir);
    //char string[] = "/home";
    char *token;
    char rutaDir2[300];
    strcpy(rutaDir2,rutaCarpeta);
    char rutaDir3[300];
    strcpy(rutaDir3,rutaCarpeta);
    token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
    if(esRaiz(rutaDir2)!=0){

    Inodo inodo;
    BloqueExt3 bloque;
    int posicionInodo=superBloque.apunInodo;
    int posicionBloque;


    //printf("rudaDir: %s\n",rutaDir3);
    token = strtok(rutaDir3 , "/");
    //return;
    int profundidad=0;
    while (token != NULL){

        if(profundidad==0){
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);
            int tipo=strcmp(inodo.tipo,"Carpeta");
            if(tipo==0){
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);

                    int cmp=strcmp(token,bloque.nombre);
                    if(cmp==0){
                        //posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                    }else{
                        printf("\n---La ruta no existe---\n\n");
                        return;
                    }
            }
        }else{
            int i;
            for(i=0;i<6;i++){
                posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");
                if(tipo==0){
                    BloqueExt3 aux=bloque;
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                   if(strcmp(bloque.nombre,token)==0){
                       break;
                   }
                   if(i==5){
                       printf("\n---La ruta no existe---\n\n");
                       break;
                   }
                   bloque=aux;
                }
            }

        }

        profundidad++;
        token = strtok(NULL,"/");
    }

    //verifica si existe el archivo en el directorio
    int rep;
    int estado=0;
    for(rep=0;rep<6;rep++){
        posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
        fseek(archivo,posicionInodo,SEEK_SET);
        fread(&inodo,sizeof(Inodo), 1, archivo);
        int tipo=strcmp(inodo.tipo,"Archivo");
        if(tipo==0){
            BloqueExt3 aux=bloque;
            posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
           if(strcmp(bloque.nombre,nombreDir)==0){
               //printf("\n---Ya existe una archivo con el mismo nombre en esta ruta---\n\n");
               //return;
               //archivo encontrado
               estado=1;
               break;
           }
           bloque=aux;

        }
    }

    if(estado==0){
        printf("\n---El archivo no existe---\n\n");
        fflush(archivoIndex);
        fclose(archivoIndex);
        fflush(archivo);
        fclose(archivo);
        return;
    }

    //lee los bloques del archivo

    int rep2;
    char *textoTemporal=(char *)malloc(inodo.tamArchivo);
    strcpy(textoTemporal,"");
    int tamanioArchivo=inodo.tamArchivo;
    for(rep2=0;rep2<5;rep2++){

            posicionBloque=superBloque.apunBloque+inodo.directo[rep2]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
            //printf("\ntexto entrada: %s ",bloque.contenido);
           if(inodo.directo[rep2]!=-1){
               //escribe los bloques en un archivo temporal para posteriormente mostralo en un
               //editor de texto
                if(tamanioArchivo<64){
                   // int i;
                   // for(i=tamanioArchivo;i<100;i++){
                     //       bloque.contenido[i]=' ';
                       // }
                }
                //printf("\n%s\n",bloque.contenido);
                strcat(textoTemporal,bloque.contenido);
                tamanioArchivo=tamanioArchivo-64;

           }
    }



    FILE *archivoLeidoTemporal=fopen("/home/jose/Documentos/Discos/archivoLeidoTemporal.txt","wb+");
    fseek(archivoLeidoTemporal,0,SEEK_SET);
    fwrite(textoTemporal,inodo.tamArchivo,1,archivoLeidoTemporal);
    //int ta=strlen(textoTemporal);
    //printf("\n tam archivo inodo %d\n",inodo.tamArchivo);
    //printf("\n tam archivo %d\n",strlen(textoTemporal));
    fflush(archivoLeidoTemporal);
    //fclose(archivoLeidoTemporal);

    system("libreoffice /home/jose/Documentos/Discos/archivoLeidoTemporal.txt");
    remove("/home/jose/Documentos/Discos/archivoLeidoTemporal.txt");
    }


    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);


}



void modificarNombreArchivoExt3(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }




    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    printf("Ingrese la ruda del archivo: ");
    char rutaDir[100];
    scanf("%s", &rutaDir);
   // printf("Nombre del del archivo: ");
    char nombreDir[50]="";
    //scanf("%s", &nombreDir);


    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);
    time_t tiempo = time(0);
    char bitInodo;
    fread(&bitInodo, 1, 1, archivo);
    //printf("%c",bitInodo);

    char *tokenDir;
    char *tokenNombre;
    char entradaRuta1[200];
    char entradaRuta2[200];
    strcpy(entradaRuta1,rutaDir);
    strcpy(entradaRuta2,rutaDir);
    char rutaCarpeta[300];
    tokenDir=strtok(entradaRuta1,"/");
    obtenerRutaCarpeta(tokenDir,rutaCarpeta);
    tokenNombre=strtok(entradaRuta2,"/");
    obtenerNombre(tokenNombre,nombreDir);
    //char string[] = "/home";
    char *token;
    char rutaDir2[300];
    strcpy(rutaDir2,rutaCarpeta);
    char rutaDir3[300];
    strcpy(rutaDir3,rutaCarpeta);
    token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
    if(esRaiz(rutaDir2)!=0){

    Inodo inodo;
    BloqueExt3 bloque;
    int posicionInodo=superBloque.apunInodo;
    int posicionBloque;


    //printf("rudaDir: %s\n",rutaDir3);
    token = strtok(rutaDir3 , "/");
    //return;
    int profundidad=0;
    while (token != NULL){

        if(profundidad==0){
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);
            int tipo=strcmp(inodo.tipo,"Carpeta");
            if(tipo==0){
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);

                    int cmp=strcmp(token,bloque.nombre);
                    if(cmp==0){
                        //posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                    }else{
                        printf("\n---La ruta no existe---\n\n");
                        return;
                    }
            }
        }else{
            int i;
            for(i=0;i<6;i++){
                posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");
                if(tipo==0){
                    BloqueExt3 aux=bloque;
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                   if(strcmp(bloque.nombre,token)==0){
                       break;
                   }
                   if(i==5){
                       printf("\n---La ruta no existe---\n\n");
                       break;
                   }
                   bloque=aux;
                }
                if(i==5){
                    printf("\n---La ruta no existe---\n\n");
                    break;
                }
            }

        }

        profundidad++;
        token = strtok(NULL,"/");
    }

    //verifica si existe el archivo en el directorio
    int rep;
    int estado=0;
    for(rep=0;rep<6;rep++){
        posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
        fseek(archivo,posicionInodo,SEEK_SET);
        fread(&inodo,sizeof(Inodo), 1, archivo);
        int tipo=strcmp(inodo.tipo,"Archivo");
        if(tipo==0){
            BloqueExt3 aux=bloque;
            posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
           if(strcmp(bloque.nombre,nombreDir)==0){
               //printf("\n---Ya existe una archivo con el mismo nombre en esta ruta---\n\n");
               //return;
               //archivo encontrado
               estado=1;
               break;
           }
           bloque=aux;

        }
    }

    if(estado==0){
        printf("\n---El archivo no existe---\n\n");
        fflush(archivoIndex);
        fclose(archivoIndex);
        fflush(archivo);
        fclose(archivo);
        return;
    }


    printf("Nuevo nombre:  ");
    char nuevoNombre[100];
    scanf("%s", &nuevoNombre);

    //lee los bloques del archivo

    int rep2;
    char *textoTemporal=(char *)malloc(inodo.tamArchivo);
    strcpy(textoTemporal,"");

    for(rep2=0;rep2<5;rep2++){

            posicionBloque=superBloque.apunBloque+inodo.directo[rep2]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
            //printf("\ntexto entrada: %s ",bloque.contenido);
           if(inodo.directo[rep2]!=-1){

                fseek(archivo,posicionBloque,SEEK_SET);
                strcpy(bloque.nombre,nuevoNombre);
                fwrite(&bloque,sizeof(BloqueExt3), 1, archivo);
           }
    }


    }


    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);


}


void obtenerNombre(char *token,char *nombreArchivo){
    int cont=0;
    while (token != NULL){
            cont++;

            //printf("El token es: %s\n",token);
            strcpy(nombreArchivo,token);
            token = strtok(NULL,"/");
}

}


void obtenerRutaCarpeta(char *token,char *ruta){
    int cont=0;
    char aux[50]="";
    while (token != NULL){
            cont++;
            strcpy(aux,token);
            //printf("El token es: %s\n",token);
            token = strtok(NULL,"/");
            if(token!=NULL){
            strcat(ruta,"/");
            strcat(ruta,aux);
            }

}

}

void listarDirectorioActual(){


        printf("Id del disco: ");
        char id[32];

        scanf("%s", &id);
        if (existeDiscoID(id) == 0) {
            printf("-----El disco no existe-----\n\n");
            return;
        }

        IndexDisco indexDisco;
        FILE *archivoIndex = fopen(rutaGeneral, "rb+");

        while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

            if (!strcmp(id, indexDisco.id)) {
                break;
            }
        }
        char nombre[32];
        printf("Nombre de la particion: ");
        scanf("%s", &nombre);
        int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
        if (tipoP == 0) {
            printf("-----La particion no existe-----\n\n");
            return;
        } else if (tipoP == -1) {
            printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
            return;
        }else if(tipoP==1){
            printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
            return;
        }



        MBR mbr;
        char ruta [60];
        strcpy(ruta, rutaCarpeta);
        strcat(ruta, "/");
        strcat(ruta, indexDisco.nombre);
        strcat(ruta, ".vd");

        FILE *archivo = fopen(ruta, "rb+");
        fread(&mbr, sizeof (MBR), 1, archivo);
        int z;
        for (z = 0; z < 16; z++) {
            if (!strcmp(nombre, mbr.particion[z].nombre)) {
                break;
            }
        }


        if(mbr.particion[z].tipoFormato!=3){
            printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
            return;
        }

        printf("Ingrese la ruda del directorio: ");
        char rutaDir[100];
        scanf("%s", &rutaDir);



        SuperBloque superBloque;
        fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
        fread(&superBloque, sizeof (SuperBloque), 1, archivo);
        time_t tiempo = time(0);
        char bitInodo;
        fread(&bitInodo, 1, 1, archivo);
        printf("%c",bitInodo);


        //char string[] = "/home";
        char *token;
        char rutaDir2[100];
        strcpy(rutaDir2,rutaDir);
        char rutaDir3[100];
        strcpy(rutaDir3,rutaDir);
        token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
        if(esRaiz(rutaDir2)==0){
                fseek(archivo,superBloque.apunInodo,SEEK_SET);

                    Inodo inodo;
                    fread(&inodo,sizeof(Inodo), 1, archivo);


                    fseek(archivo,superBloque.apunBloque,SEEK_SET);
                    BloqueExt3 bloque;
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                    printf("\n=======================================\n");
                    printf("\%s\n",bloque.nombre);
                    printf("\n=======================================\n");

        }else{

        Inodo inodo;
        BloqueExt3 bloque;
        int posicionInodo=superBloque.apunInodo;
        int posicionBloque;


        //printf("rudaDir: %s\n",rutaDir3);
        token = strtok(rutaDir3 , "/");
        //return;
        int profundidad=0;
        while (token != NULL){

            if(profundidad==0){
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");
                if(tipo==0){
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                        int cmp=strcmp(token,bloque.nombre);
                        if(cmp==0){
                            //posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                        }else{
                            printf("\n---La ruta no existe---\n\n");
                            return;
                        }
                }
            }else{
                int i;
                for(i=0;i<6;i++){
                    posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                    fseek(archivo,posicionInodo,SEEK_SET);
                    fread(&inodo,sizeof(Inodo), 1, archivo);
                    int tipo=strcmp(inodo.tipo,"Carpeta");
                    if(tipo==0){
                        BloqueExt3 aux=bloque;
                        posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                        fseek(archivo,posicionBloque,SEEK_SET);
                        fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                       if(strcmp(bloque.nombre,token)==0){
                           break;
                       }
                       if(i==5){
                           printf("\n---La ruta no existe---\n\n");
                           break;
                       }
                       bloque=aux;
                    }
                    if(i==5){
                        printf("\n---La ruta no existe---\n\n");
                        break;
                    }
                }

            }

            profundidad++;
            token = strtok(NULL,"/");
        }

        //verifica si existe una carpeta con el mismo nombre en el directorio
        printf("\n=======================================\n");
        int rep;
        for(rep=0;rep<6;rep++){
            posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);


                BloqueExt3 aux=bloque;
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                if(aux.apuntadores[rep]!=-1){
                printf("\%s\n",bloque.nombre);
                }
                bloque=aux;

        }

        printf("\n=======================================\n");
        }

        fflush(archivoIndex);
        fclose(archivoIndex);
        fflush(archivo);
        fclose(archivo);

}

void buscarDirectorioArchivo(int tipo){


        printf("Id del disco: ");
        char id[32];

        scanf("%s", &id);
        if (existeDiscoID(id) == 0) {
            printf("-----El disco no existe-----\n\n");
            return;
        }

        IndexDisco indexDisco;
        FILE *archivoIndex = fopen(rutaGeneral, "rb+");

        while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

            if (!strcmp(id, indexDisco.id)) {
                break;
            }
        }
        char nombre[32];
        printf("Nombre de la particion: ");
        scanf("%s", &nombre);
        int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
        if (tipoP == 0) {
            printf("-----La particion no existe-----\n\n");
            return;
        } else if (tipoP == -1) {
            printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
            return;
        }else if(tipoP==1){
            printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
            return;
        }



        MBR mbr;
        char ruta [60];
        strcpy(ruta, rutaCarpeta);
        strcat(ruta, "/");
        strcat(ruta, indexDisco.nombre);
        strcat(ruta, ".vd");

        FILE *archivo = fopen(ruta, "rb+");
        fread(&mbr, sizeof (MBR), 1, archivo);
        int z;
        for (z = 0; z < 16; z++) {
            if (!strcmp(nombre, mbr.particion[z].nombre)) {
                break;
            }
        }

        if(mbr.particion[z].tipoFormato!=3){
            printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
            return;
        }

        printf("Nombre del directorio: ");
        char nombreDir[50];
        scanf("%s", &nombreDir);



        SuperBloque superBloque;
        fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
        fread(&superBloque, sizeof (SuperBloque), 1, archivo);

        int i;
        int pos=mbr.particion[z].inicio+sizeof(SuperBloque)+2*superBloque.numeroBloques+superBloque.numeroBloques*sizeof(Inodo)+superBloque.numeroBloques*sizeof(BloqueExt3);
        fseek(archivo,pos,SEEK_SET);
        int estadoBusqueda=0;
        printf("\n=============================================\n");
        for(i=0;i<150;i++){

            Log bitacora;
            fread(&bitacora, sizeof (Log), 1, archivo);
            int tipo1=strcmp(bitacora.tipoOperacion,"Creacion de archivo");
            int tipo2=strcmp(bitacora.tipoOperacion,"Creacion de carpeta");
            if(bitacora.tipo!=-1&tipo==1&tipo2==0){
               char path1[200];
               char path2[200];
               strcpy(path1,bitacora.nombre);
               strcpy(path2,bitacora.nombre);
               char *token;
               token=strtok(path1,"/");
               char nombre[50];
               obtenerNombre(token,nombre);
               int nom=strcmp(nombreDir,nombre);
               if(nom==0){
                   estadoBusqueda++;
                   char *token2;
                   token2=strtok(path2,"/");
                   char rutaBusqueda[200];
                   strcpy(rutaBusqueda,"");
                   while (token2 != NULL){
                      strcat(rutaBusqueda,"/");
                      strcat(rutaBusqueda,token2);
                      printf("\n%s",rutaBusqueda);
                      token2 = strtok(NULL,"/");
               }
                   printf("\n------------------------------------\n");
               }
            }else if(bitacora.tipo!=-1&tipo==0&tipo1==0){
               char path1[200];
               char path2[200];
               strcpy(path1,bitacora.nombre);
               strcpy(path2,bitacora.nombre);
               char *token;
               token=strtok(path1,"/");
               char nombre[50];
               obtenerNombre(token,nombre);
               int nom=strcmp(nombreDir,nombre);
               if(nom==0){
                   estadoBusqueda++;
                   char *token2;
                   token2=strtok(path2,"/");
                   char rutaBusqueda[200];
                   strcpy(rutaBusqueda,"");
                   while (token2 != NULL){
                      strcat(rutaBusqueda,"/");
                      strcat(rutaBusqueda,token2);
                      printf("\n%s",rutaBusqueda);
                      token2 = strtok(NULL,"/");
               }
                   printf("\n------------------------------------\n");
               }
            }

        }
        if(estadoBusqueda==0){
            printf("\n---No existe---\n\n");
        }
        printf("\n=============================================\n");
        printf("\n");

        fflush(archivoIndex);
        fclose(archivoIndex);
        fflush(archivo);
        fclose(archivo);



}

void eliminarArchivoExt3(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }



    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    printf("Ingrese la ruda del directorio: ");
    char rutaDir[100];
    scanf("%s", &rutaDir);
    printf("Nombre del del archivo: ");
    char nombreDir[50];
    scanf("%s", &nombreDir);
    char path [200];
    strcpy(path,rutaDir);
    strcat(path,"/");
    strcat(path,nombreDir);

    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);
    time_t tiempo = time(0);
    char bitInodo;
    fread(&bitInodo, 1, 1, archivo);
    printf("%c",bitInodo);


    //char string[] = "/home";
    char *token;
    char rutaDir2[100];
    strcpy(rutaDir2,rutaDir);
    char rutaDir3[100];
    strcpy(rutaDir3,rutaDir);
    token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
    if(esRaiz(rutaDir2)!=0){

    Inodo inodo;
    BloqueExt3 bloque;
    int posicionInodo=superBloque.apunInodo;
    int posicionBloque;


    //printf("rudaDir: %s\n",rutaDir3);
    token = strtok(rutaDir3 , "/");
    //return;
    int profundidad=0;
    while (token != NULL){

        if(profundidad==0){
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);
            int tipo=strcmp(inodo.tipo,"Carpeta");
            if(tipo==0){
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                    int cmp=strcmp(token,bloque.nombre);
                    if(cmp==0){
                        //posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                    }else{
                        printf("\n---La ruta no existe---\n\n");
                        return;
                    }
            }
        }else{
            int i;
            for(i=0;i<6;i++){
                posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");
                if(tipo==0){
                    BloqueExt3 aux=bloque;
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                   if(strcmp(bloque.nombre,token)==0){
                       break;
                   }
                   if(i==5){
                       printf("\n---La ruta no existe---\n\n");
                       return;
                   }
                   bloque=aux;
                }

                if(i==5){
                    printf("\n---La ruta no existe---\n\n");
                    return;
                }
            }

        }

        profundidad++;
        token = strtok(NULL,"/");
    }

    //verifica si existe una carpeta con el mismo nombre en el directorio
    int rep;
    int posicionBloqueActual=posicionBloque;
    BloqueExt3 bloqueCarpeta=bloque;
    for(rep=0;rep<6;rep++){
        posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
        fseek(archivo,posicionInodo,SEEK_SET);
        fread(&inodo,sizeof(Inodo), 1, archivo);
        Inodo inodoAux=inodo;
        int tipo=strcmp(inodo.tipo,"Archivo");
        if(tipo==0){
            BloqueExt3 aux=bloque;
            posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
           if(strcmp(bloque.nombre,nombreDir)==0){

               break;
           }
           bloque=aux;
           inodo=inodoAux;
        }
    }



        fseek(archivo,superBloque.apunBitMatFicheros+bloqueCarpeta.apuntadores[rep],SEEK_SET);
        char c='0';
        fwrite(&c,1, 1, archivo);
        fseek(archivo,superBloque.apunInodo+8,SEEK_SET);
        char ca;
        fread(&ca,1, 1, archivo);
        fseek(archivo,posicionBloqueActual,SEEK_SET);
        //int posInodoElim=bloque.apuntadores[rep];
        bloqueCarpeta.apuntadores[rep]=-1;

        fwrite(&bloqueCarpeta,sizeof(BloqueExt3), 1, archivo);

    //Inodo inodoNew;


    int j;
    char contenidoEliminado[200];
    strcpy(contenidoEliminado,"");
    for(j=0;j<6;j++){
        if(inodo.directo[j]!=-1){
        fseek(archivo,superBloque.apunBloque+inodo.directo[j]*sizeof(BloqueExt3),SEEK_SET);
        BloqueExt3 bloque;
        fread(&bloque,sizeof(BloqueExt3), 1, archivo);
        strcat(contenidoEliminado,bloque.contenido);
        }
    }


    for(j=0;j<6;j++){
        if(inodo.directo[j]!=-1){
        fseek(archivo,superBloque.apunBitMatFicheros+superBloque.numeroInodos+inodo.directo[j],SEEK_SET);
        char c='0';
        fwrite(&c,1, 1, archivo);
        inodo.directo[j]=-1;
        }
    }
    fseek(archivo,posicionInodo,SEEK_SET);
    fwrite(&inodo,sizeof(Inodo), 1, archivo);

    Log bitacora;
    strcpy(bitacora.nombre,path);
    strcpy(bitacora.tipoOperacion,"Eliminacion archivo");
    bitacora.tipo=0;
    bitacora.fecha=localtime(&tiempo);
    strcpy(bitacora.contenido,contenidoEliminado);
    fseek(archivo,superBloque.apunBitacora,SEEK_SET);
    fwrite(&bitacora,sizeof(Log), 1, archivo);

    modificarSuperBloque(archivo,&superBloque,&mbr.particion[z],0);
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    superBloque.apunBitacora=superBloque.apunBitacora+sizeof(Log);
    fwrite(&superBloque,sizeof(SuperBloque), 1, archivo);




    }

    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);

}



void rutaInodosBloques(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }




    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    char rutaCarpeta2[200];
    strcpy(rutaCarpeta2,rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    printf("Ingrese la ruda del directorio: ");
    char rutaDir[100];
    scanf("%s", &rutaDir);
   // printf("Nombre del del archivo: ");
    char nombreDir[50]="";
    //scanf("%s", &nombreDir);


    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);
    time_t tiempo = time(0);
    char bitInodo;
    fread(&bitInodo, 1, 1, archivo);
    //printf("%c",bitInodo);

    char *tokenDir;
    char *tokenNombre;
    char entradaRuta1[200];
    char entradaRuta2[200];
    strcpy(entradaRuta1,rutaDir);
    strcpy(entradaRuta2,rutaDir);
    char rutaCarpeta[300];
    strcat(rutaCarpeta,entradaRuta1);
    tokenDir=strtok(entradaRuta1,"/");

    //obtenerRutaCarpeta(tokenDir,rutaCarpeta);
    tokenNombre=strtok(entradaRuta2,"/");
    obtenerNombre(tokenNombre,nombreDir);
    //char string[] = "/home";
    char *token;
    char rutaDir2[300];
    strcpy(rutaDir2,rutaCarpeta);
    char rutaDir3[300];
    strcpy(rutaDir3,rutaCarpeta);
    token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
    if(esRaiz(rutaDir2)!=0){

    Inodo inodo;
    BloqueExt3 bloque;
    int posicionInodo=superBloque.apunInodo;
    int posicionBloque;



    char rutaDot[60];//ruta para crear el archivo .dot
    strcpy(rutaDot, rutaCarpeta2);
    strcat(rutaDot, "/RutaInodosBloques.dot");
    FILE *reporteD = fopen(rutaDot, "w");
    char cadenaDot[10000] = "";
    char punteros[10000]="";
    strcat(cadenaDot, "digraph tabla { ");
    char aux[30];


    //printf("rudaDir: %s\n",rutaDir3);
    token = strtok(rutaCarpeta, "/");
    //return;
    int profundidad=0;

    while (token != NULL){

        if(profundidad==0){
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);
            int tipo=strcmp(inodo.tipo,"Carpeta");
            if(tipo==0){
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);

                    int cmp=strcmp(token,bloque.nombre);
                    if(cmp==0){

                        //crea nodos inodos XD
                        strcat(cadenaDot,"node [shape=record];");

                        strcat(cadenaDot,"inodo");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"[label=\"{");

                        strcat(cadenaDot,"Inodo ");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"ID= ");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Tamanio= ");
                        sprintf(aux,"%d",inodo.tamArchivo);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Tipo= ");
                        strcat(cadenaDot,inodo.tipo);
                        strcat(cadenaDot,"|");
                        int k;
                        for( k=0;k<5;k++){
                            strcat(cadenaDot,"Ap");
                            sprintf(aux,"%d",k);
                            strcat(cadenaDot,aux);
                            strcat(cadenaDot," = ");
                            if(inodo.directo[k]==-1){
                                strcat(cadenaDot,"null");
                            }else{
                                sprintf(aux,"%d",inodo.directo[k]);
                            strcat(cadenaDot,aux);
                            }
                            strcat(cadenaDot,"|");
                        }

                        for(k=0;k<2;k++){
                            strcat(cadenaDot,"Api");
                            sprintf(aux,"%d",k);
                            strcat(cadenaDot,aux);
                            strcat(cadenaDot," = ");
                            if(inodo.indirecto[k]==-1){
                                strcat(cadenaDot,"null");
                            }else{
                                sprintf(aux,"%d",inodo.indirecto[k]);
                            strcat(cadenaDot,aux);
                            }
                            if(k<1){
                              strcat(cadenaDot,"|");
                            }

                        }



                        strcat(cadenaDot,"}\"];");

                        //crea nodos bloques
                        strcat(cadenaDot,"node [shape=record];");

                        strcat(cadenaDot,"bloque");
                        sprintf(aux,"%d",bloque.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"[label=\"{");

                        strcat(cadenaDot,"Bloque ");
                        sprintf(aux,"%d",bloque.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"ID= ");
                        sprintf(aux,"%d",bloque.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Padre= ");
                        strcat(cadenaDot,bloque.padre);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Nombre= ");
                        strcat(cadenaDot,bloque.nombre);
                        strcat(cadenaDot,"|");

                        int b;
                        for( b=0;b<6;b++){
                            strcat(cadenaDot,"Hijo");
                            sprintf(aux,"%d",b);
                            strcat(cadenaDot,aux);
                            strcat(cadenaDot," = ");
                            if(bloque.apuntadores[b]==-1){
                                strcat(cadenaDot,"null");
                            }else{
                                sprintf(aux,"%d",bloque.apuntadores[b]);
                            strcat(cadenaDot,aux);
                            }
                            if(b<5){
                              strcat(cadenaDot,"|");
                            }

                        }





                        //crea los enlaces de inodos a bloques
                        strcat(cadenaDot,"}\"];");
                        strcat(punteros,"inodo");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(punteros,aux);
                        strcat(punteros,"->");
                        strcat(punteros,"bloque");
                        sprintf(aux,"%d",inodo.directo[0]);
                        strcat(punteros,aux);
                        strcat(punteros,";");




                    }else{
                        printf("\n---La ruta no existe---\n\n");
                        return;
                    }
            }
        }else{
            int i;
            for(i=0;i<6;i++){
                posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");


                //crea apuntadores de bloques a inodos
                if(bloque.apuntadores[i]!=-1){
                    strcat(punteros,"bloque");
                    sprintf(aux,"%d",bloque.llave);
                    strcat(punteros,aux);
                    strcat(punteros,"->");
                    strcat(punteros,"inodo");
                    sprintf(aux,"%d",bloque.apuntadores[i]);
                    strcat(punteros,aux);
                    strcat(punteros,";");

                }



                if(tipo==0){
                    BloqueExt3 aux2=bloque;
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                   if(strcmp(bloque.nombre,token)==0){




                       //crea nodos inodos XD
                       strcat(cadenaDot,"node [shape=record];");

                       strcat(cadenaDot,"inodo");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"[label=\"{");

                       strcat(cadenaDot,"Inodo ");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"ID= ");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Tamanio= ");
                       sprintf(aux,"%d",inodo.tamArchivo);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Tipo= ");
                       strcat(cadenaDot,inodo.tipo);
                       strcat(cadenaDot,"|");
                       int k;
                       for( k=0;k<5;k++){
                           strcat(cadenaDot,"Ap");
                           sprintf(aux,"%d",k);
                           strcat(cadenaDot,aux);
                           strcat(cadenaDot," = ");
                           if(inodo.directo[k]==-1){
                               strcat(cadenaDot,"null");
                           }else{
                               sprintf(aux,"%d",inodo.directo[k]);
                           strcat(cadenaDot,aux);
                           }
                           strcat(cadenaDot,"|");
                       }

                       for(k=0;k<2;k++){
                           strcat(cadenaDot,"Api");
                           sprintf(aux,"%d",k);
                           strcat(cadenaDot,aux);
                           strcat(cadenaDot," = ");
                           if(inodo.indirecto[k]==-1){
                               strcat(cadenaDot,"null");
                           }else{
                               sprintf(aux,"%d",inodo.indirecto[k]);
                           strcat(cadenaDot,aux);
                           }
                           if(k<1){
                             strcat(cadenaDot,"|");
                           }

                       }



                        strcat(cadenaDot,"}\"];");

                       //crea nodos bloques
                       strcat(cadenaDot,"node [shape=record];");

                       strcat(cadenaDot,"bloque");
                       sprintf(aux,"%d",bloque.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"[label=\"{");

                       strcat(cadenaDot,"Bloque ");
                       sprintf(aux,"%d",bloque.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"ID= ");
                       sprintf(aux,"%d",bloque.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Padre= ");
                       strcat(cadenaDot,bloque.padre);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Nombre= ");
                       strcat(cadenaDot,bloque.nombre);
                       strcat(cadenaDot,"|");

                       int b;
                       for( b=0;b<6;b++){
                           strcat(cadenaDot,"Hijo");
                           sprintf(aux,"%d",b);
                           strcat(cadenaDot,aux);
                           strcat(cadenaDot," = ");
                           if(bloque.apuntadores[b]==-1){
                               strcat(cadenaDot,"null");
                           }else{
                               sprintf(aux,"%d",bloque.apuntadores[b]);
                           strcat(cadenaDot,aux);
                           }
                           if(b<5){
                             strcat(cadenaDot,"|");
                           }

                       }





                       //crea los enlaces de inodos a bloques
                       strcat(cadenaDot,"}\"];");
                       strcat(punteros,"inodo");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(punteros,aux);
                       strcat(punteros,"->");
                       strcat(punteros,"bloque");
                       sprintf(aux,"%d",inodo.directo[0]);
                       strcat(punteros,aux);
                       strcat(punteros,";");

                       break;
                   }
                   if(i==5){
                       printf("\n---La ruta no existe---\n\n");
                       break;
                   }
                   bloque=aux2;
                }

                if(i==5){
                    printf("\n---La ruta no existe---\n\n");
                    break;
                }
            }

        }

        profundidad++;
        token = strtok(NULL,"/");
    }

    strcat(cadenaDot,punteros);
    strcat(cadenaDot,"}");
    fprintf(reporteD, "%s", cadenaDot);
    fflush(reporteD);
    //fclose(reporteD);

    //instrucciones para crear la imagen png desde el archivo .dot
            char rutaDot2 [1000];
            strcpy(rutaDot2, "dot -Tpng ");
            strcat(rutaDot2, rutaDot);
            strcat(rutaDot2, " -o ");
            strcat(rutaDot2, rutaCarpeta2);
            strcat(rutaDot2, "/RutaInodosBloques.png");
            system(rutaDot2);
            //abre la imagen con el editor de imagenes shotwell
            char rutaPNG [1000];
            strcpy(rutaPNG, "shotwell ");
            strcat(rutaPNG, rutaCarpeta2);
            strcat(rutaPNG, "/RutaInodosBloques.png");
            system(rutaPNG);

            return;
            // quitar return para continuar graficando los bloques de lso archivos
    //verifica si existe el archivo en el directorio
    int rep;
    int estado=0;
    for(rep=0;rep<6;rep++){
        posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
        fseek(archivo,posicionInodo,SEEK_SET);
        fread(&inodo,sizeof(Inodo), 1, archivo);
        int tipo=strcmp(inodo.tipo,"Archivo");
        if(tipo==0){
            BloqueExt3 aux=bloque;
            posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
           if(strcmp(bloque.nombre,nombreDir)==0){
               //printf("\n---Ya existe una archivo con el mismo nombre en esta ruta---\n\n");
               //return;
               //archivo encontrado
               estado=1;
               break;
           }
           bloque=aux;

        }
    }

    if(estado==0){
        printf("\n---El archivo no existe---\n\n");
        fflush(archivoIndex);
        fclose(archivoIndex);
        fflush(archivo);
        fclose(archivo);
        return;
    }

    //lee los bloques del archivo

    int rep2;
    char *textoTemporal=(char *)malloc(inodo.tamArchivo);
    strcpy(textoTemporal,"");
    int tamanioArchivo=inodo.tamArchivo;
    for(rep2=0;rep2<5;rep2++){

            posicionBloque=superBloque.apunBloque+inodo.directo[rep2]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
            //printf("\ntexto entrada: %s ",bloque.contenido);
           if(inodo.directo[rep2]!=-1){
               //escribe los bloques en un archivo temporal para posteriormente mostralo en un
               //editor de texto
                if(tamanioArchivo<64){
                   // int i;
                   // for(i=tamanioArchivo;i<100;i++){
                     //       bloque.contenido[i]=' ';
                       // }
                }
                //printf("\n%s\n",bloque.contenido);
                strcat(textoTemporal,bloque.contenido);
                tamanioArchivo=tamanioArchivo-64;

           }
    }



    FILE *archivoLeidoTemporal=fopen("/home/jose/Documentos/Discos/archivoLeidoTemporal.txt","wb+");
    fseek(archivoLeidoTemporal,0,SEEK_SET);
    fwrite(textoTemporal,inodo.tamArchivo,1,archivoLeidoTemporal);
    //int ta=strlen(textoTemporal);
    //printf("\n tam archivo inodo %d\n",inodo.tamArchivo);
    //printf("\n tam archivo %d\n",strlen(textoTemporal));
    fflush(archivoLeidoTemporal);
    //fclose(archivoLeidoTemporal);

    system("libreoffice /home/jose/Documentos/Discos/archivoLeidoTemporal.txt");
    remove("/home/jose/Documentos/Discos/archivoLeidoTemporal.txt");
    }


    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);


}









void rutaInodosBloquesArchivo(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }




    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    char rutaCarpeta2[200];
    strcpy(rutaCarpeta2,rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    printf("Ingrese la ruda del directorio: ");
    char rutaDir[100];
    scanf("%s", &rutaDir);
   // printf("Nombre del del archivo: ");
    char nombreDir[50]="";
    //scanf("%s", &nombreDir);


    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);
    time_t tiempo = time(0);
    char bitInodo;
    fread(&bitInodo, 1, 1, archivo);
    //printf("%c",bitInodo);

    char *tokenDir;
    char *tokenNombre;
    char entradaRuta1[200];
    char entradaRuta2[200];
    strcpy(entradaRuta1,rutaDir);
    strcpy(entradaRuta2,rutaDir);
    char rutaCarpeta[300];
    //strcat(rutaCarpeta,entradaRuta1);
    tokenDir=strtok(entradaRuta1,"/");

    obtenerRutaCarpeta(tokenDir,rutaCarpeta);
    char rutaCarpeta3[200];
    strcpy(rutaCarpeta3,rutaCarpeta);
    tokenNombre=strtok(entradaRuta2,"/");
    obtenerNombre(tokenNombre,nombreDir);
    //char string[] = "/home";
    char *token;
    char rutaDir2[300];
    strcpy(rutaDir2,rutaCarpeta);
    char rutaDir3[300];
    strcpy(rutaDir3,rutaCarpeta);
    token = strtok(rutaDir2 , "/"); /*Separamos por diagonal*/
    char rutaDot[60];//ruta para crear el archivo .dot
    char cadenaDot[10000] = "";
    char punteros[10000]="";
    if(esRaiz(rutaDir2)!=0){

    Inodo inodo;
    BloqueExt3 bloque;
    int posicionInodo=superBloque.apunInodo;
    int posicionBloque;




    strcpy(rutaDot, rutaCarpeta2);
    strcat(rutaDot, "/RutaInodosBloques.dot");
    FILE *reporteD = fopen(rutaDot, "w");

    strcat(cadenaDot, "digraph tabla { ");
    char aux[30];


    //printf("rudaDir: %s\n",rutaDir3);
    token = strtok(rutaCarpeta, "/");
    //return;
    int profundidad=0;

    while (token != NULL){

        if(profundidad==0){
            fseek(archivo,posicionInodo,SEEK_SET);
            fread(&inodo,sizeof(Inodo), 1, archivo);
            int tipo=strcmp(inodo.tipo,"Carpeta");
            if(tipo==0){
                posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                fseek(archivo,posicionBloque,SEEK_SET);
                fread(&bloque,sizeof(BloqueExt3), 1, archivo);

                    int cmp=strcmp(token,bloque.nombre);
                    if(cmp==0){

                        //crea nodos inodos XD
                        strcat(cadenaDot,"node [shape=record];");

                        strcat(cadenaDot,"inodo");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"[label=\"{");

                        strcat(cadenaDot,"Inodo ");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"ID= ");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Tamanio= ");
                        sprintf(aux,"%d",inodo.tamArchivo);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Tipo= ");
                        strcat(cadenaDot,inodo.tipo);
                        strcat(cadenaDot,"|");
                        int k;
                        for( k=0;k<5;k++){
                            strcat(cadenaDot,"Ap");
                            sprintf(aux,"%d",k);
                            strcat(cadenaDot,aux);
                            strcat(cadenaDot," = ");
                            if(inodo.directo[k]==-1){
                                strcat(cadenaDot,"null");
                            }else{
                                sprintf(aux,"%d",inodo.directo[k]);
                            strcat(cadenaDot,aux);
                            }
                            strcat(cadenaDot,"|");
                        }

                        for(k=0;k<2;k++){
                            strcat(cadenaDot,"Api");
                            sprintf(aux,"%d",k);
                            strcat(cadenaDot,aux);
                            strcat(cadenaDot," = ");
                            if(inodo.indirecto[k]==-1){
                                strcat(cadenaDot,"null");
                            }else{
                                sprintf(aux,"%d",inodo.indirecto[k]);
                            strcat(cadenaDot,aux);
                            }
                            if(k<1){
                              strcat(cadenaDot,"|");
                            }

                        }



                        strcat(cadenaDot,"}\"];");

                        //crea nodos bloques
                        strcat(cadenaDot,"node [shape=record];");

                        strcat(cadenaDot,"bloque");
                        sprintf(aux,"%d",bloque.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"[label=\"{");

                        strcat(cadenaDot,"Bloque ");
                        sprintf(aux,"%d",bloque.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"ID= ");
                        sprintf(aux,"%d",bloque.llave);
                        strcat(cadenaDot,aux);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Padre= ");
                        strcat(cadenaDot,bloque.padre);
                        strcat(cadenaDot,"|");

                        strcat(cadenaDot,"Nombre= ");
                        strcat(cadenaDot,bloque.nombre);
                        strcat(cadenaDot,"|");

                        int b;
                        for( b=0;b<6;b++){
                            strcat(cadenaDot,"Hijo");
                            sprintf(aux,"%d",b);
                            strcat(cadenaDot,aux);
                            strcat(cadenaDot," = ");
                            if(bloque.apuntadores[b]==-1){
                                strcat(cadenaDot,"null");
                            }else{
                                sprintf(aux,"%d",bloque.apuntadores[b]);
                            strcat(cadenaDot,aux);
                            }
                            if(b<5){
                              strcat(cadenaDot,"|");
                            }

                        }





                        //crea los enlaces de inodos a bloques
                        strcat(cadenaDot,"}\"];");
                        strcat(punteros,"inodo");
                        sprintf(aux,"%d",inodo.llave);
                        strcat(punteros,aux);
                        strcat(punteros,"->");
                        strcat(punteros,"bloque");
                        sprintf(aux,"%d",inodo.directo[0]);
                        strcat(punteros,aux);
                        strcat(punteros,";");




                    }else{
                        printf("\n---La ruta no existe---\n\n");
                        return;
                    }
            }
        }else{
            int i;
            for(i=0;i<6;i++){
                posicionInodo=superBloque.apunInodo+bloque.apuntadores[i]*sizeof(Inodo);
                fseek(archivo,posicionInodo,SEEK_SET);
                fread(&inodo,sizeof(Inodo), 1, archivo);
                int tipo=strcmp(inodo.tipo,"Carpeta");


                //crea apuntadores de bloques a inodos
                if(bloque.apuntadores[i]!=-1){
                    strcat(punteros,"bloque");
                    sprintf(aux,"%d",bloque.llave);
                    strcat(punteros,aux);
                    strcat(punteros,"->");
                    strcat(punteros,"inodo");
                    sprintf(aux,"%d",bloque.apuntadores[i]);
                    strcat(punteros,aux);
                    strcat(punteros,";");

                }



                if(tipo==0){
                    BloqueExt3 aux2=bloque;
                    posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
                    fseek(archivo,posicionBloque,SEEK_SET);
                    fread(&bloque,sizeof(BloqueExt3), 1, archivo);
                   if(strcmp(bloque.nombre,token)==0){




                       //crea nodos inodos XD
                       strcat(cadenaDot,"node [shape=record];");

                       strcat(cadenaDot,"inodo");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"[label=\"{");

                       strcat(cadenaDot,"Inodo ");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"ID= ");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Tamanio= ");
                       sprintf(aux,"%d",inodo.tamArchivo);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Tipo= ");
                       strcat(cadenaDot,inodo.tipo);
                       strcat(cadenaDot,"|");
                       int k;
                       for( k=0;k<5;k++){
                           strcat(cadenaDot,"Ap");
                           sprintf(aux,"%d",k);
                           strcat(cadenaDot,aux);
                           strcat(cadenaDot," = ");
                           if(inodo.directo[k]==-1){
                               strcat(cadenaDot,"null");
                           }else{
                               sprintf(aux,"%d",inodo.directo[k]);
                           strcat(cadenaDot,aux);
                           }
                           strcat(cadenaDot,"|");
                       }

                       for(k=0;k<2;k++){
                           strcat(cadenaDot,"Api");
                           sprintf(aux,"%d",k);
                           strcat(cadenaDot,aux);
                           strcat(cadenaDot," = ");
                           if(inodo.indirecto[k]==-1){
                               strcat(cadenaDot,"null");
                           }else{
                               sprintf(aux,"%d",inodo.indirecto[k]);
                           strcat(cadenaDot,aux);
                           }
                           if(k<1){
                             strcat(cadenaDot,"|");
                           }

                       }



                        strcat(cadenaDot,"}\"];");

                       //crea nodos bloques
                       strcat(cadenaDot,"node [shape=record];");

                       strcat(cadenaDot,"bloque");
                       sprintf(aux,"%d",bloque.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"[label=\"{");

                       strcat(cadenaDot,"Bloque ");
                       sprintf(aux,"%d",bloque.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"ID= ");
                       sprintf(aux,"%d",bloque.llave);
                       strcat(cadenaDot,aux);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Padre= ");
                       strcat(cadenaDot,bloque.padre);
                       strcat(cadenaDot,"|");

                       strcat(cadenaDot,"Nombre= ");
                       strcat(cadenaDot,bloque.nombre);
                       strcat(cadenaDot,"|");

                       int b;
                       for( b=0;b<6;b++){
                           strcat(cadenaDot,"Hijo");
                           sprintf(aux,"%d",b);
                           strcat(cadenaDot,aux);
                           strcat(cadenaDot," = ");
                           if(bloque.apuntadores[b]==-1){
                               strcat(cadenaDot,"null");
                           }else{
                               sprintf(aux,"%d",bloque.apuntadores[b]);
                           strcat(cadenaDot,aux);
                           }
                           if(b<5){
                             strcat(cadenaDot,"|");
                           }

                       }





                       //crea los enlaces de inodos a bloques
                       strcat(cadenaDot,"}\"];");
                       strcat(punteros,"inodo");
                       sprintf(aux,"%d",inodo.llave);
                       strcat(punteros,aux);
                       strcat(punteros,"->");
                       strcat(punteros,"bloque");
                       sprintf(aux,"%d",inodo.directo[0]);
                       strcat(punteros,aux);
                       strcat(punteros,";");

                       break;
                   }
                   if(i==5){
                       printf("\n---La ruta no existe---\n\n");
                       break;
                   }
                   bloque=aux2;
                }

                if(i==5){
                    printf("\n---La ruta no existe---\n\n");
                    break;
                }
            }

        }

        profundidad++;
        token = strtok(NULL,"/");
    }




            // quitar return para continuar graficando los bloques de lso archivos
    //verifica si existe el archivo en el directorio
    int rep;
    int estado=0;
    for(rep=0;rep<6;rep++){
        posicionInodo=superBloque.apunInodo+bloque.apuntadores[rep]*sizeof(Inodo);
        fseek(archivo,posicionInodo,SEEK_SET);
        fread(&inodo,sizeof(Inodo), 1, archivo);
        int tipo=strcmp(inodo.tipo,"Archivo");
        if(tipo==0){
            BloqueExt3 aux=bloque;
            posicionBloque=superBloque.apunBloque+inodo.directo[0]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
           if(strcmp(bloque.nombre,nombreDir)==0){
               //printf("\n---Ya existe una archivo con el mismo nombre en esta ruta---\n\n");
               //return;
               //archivo encontrado
               strcat(punteros,"bloque");
               char aux2[5];
               sprintf(aux2,"%d",aux.llave);
               strcat(punteros,aux2);
               strcat(punteros,"->");
               strcat(punteros,"inodo");
               sprintf(aux2,"%d",aux.apuntadores[rep]);
               strcat(punteros,aux2);
               strcat(punteros,";");
               estado=1;
               break;
           }
           bloque=aux;

        }
    }

    if(estado==0){
        printf("\n---El archivo no existe---\n\n");
        fflush(archivoIndex);
        fclose(archivoIndex);
        fflush(archivo);
        fclose(archivo);
        return;
    }

    //lee los bloques del archivo





    //crea nodos inodos XD
    strcat(cadenaDot,"node [shape=record];");

    strcat(cadenaDot,"inodo");
    sprintf(aux,"%d",inodo.llave);
    strcat(cadenaDot,aux);
    strcat(cadenaDot,"[label=\"{");

    strcat(cadenaDot,"Inodo ");
    sprintf(aux,"%d",inodo.llave);
    strcat(cadenaDot,aux);
    strcat(cadenaDot,"|");

    strcat(cadenaDot,"ID= ");
    sprintf(aux,"%d",inodo.llave);
    strcat(cadenaDot,aux);
    strcat(cadenaDot,"|");

    strcat(cadenaDot,"Tamanio= ");
    sprintf(aux,"%d",inodo.tamArchivo);
    strcat(cadenaDot,aux);
    strcat(cadenaDot,"|");

    strcat(cadenaDot,"Tipo= ");
    strcat(cadenaDot,inodo.tipo);
    strcat(cadenaDot,"|");
    int k;
    for( k=0;k<5;k++){
        strcat(cadenaDot,"Ap");
        sprintf(aux,"%d",k);
        strcat(cadenaDot,aux);
        strcat(cadenaDot," = ");
        if(inodo.directo[k]==-1){
            strcat(cadenaDot,"null");
        }else{
            sprintf(aux,"%d",inodo.directo[k]);
        strcat(cadenaDot,aux);
        }
        strcat(cadenaDot,"|");
    }

    for(k=0;k<2;k++){
        strcat(cadenaDot,"Api");
        sprintf(aux,"%d",k);
        strcat(cadenaDot,aux);
        strcat(cadenaDot," = ");
        if(inodo.indirecto[k]==-1){
            strcat(cadenaDot,"null");
        }else{
            sprintf(aux,"%d",inodo.indirecto[k]);
        strcat(cadenaDot,aux);
        }
        if(k<1){
          strcat(cadenaDot,"|");
        }

    }



     strcat(cadenaDot,"}\"];");





    int rep2;
    char *textoTemporal=(char *)malloc(inodo.tamArchivo);
    strcpy(textoTemporal,"");
    int tamanioArchivo=inodo.tamArchivo;
    for(rep2=0;rep2<5;rep2++){

            posicionBloque=superBloque.apunBloque+inodo.directo[rep2]*sizeof(BloqueExt3);
            fseek(archivo,posicionBloque,SEEK_SET);
            fread(&bloque,sizeof(BloqueExt3), 1, archivo);
            //printf("\ntexto entrada: %s ",bloque.contenido);
           if(inodo.directo[rep2]!=-1){

                strcat(textoTemporal,bloque.contenido);
                tamanioArchivo=tamanioArchivo-64;





                //crea nodos bloques
                strcat(cadenaDot,"node [shape=record];");

                strcat(cadenaDot,"bloque");
                sprintf(aux,"%d",bloque.llave);
                strcat(cadenaDot,aux);
                strcat(cadenaDot,"[label=\"{");

                strcat(cadenaDot,"Bloque ");
                sprintf(aux,"%d",bloque.llave);
                strcat(cadenaDot,aux);
                strcat(cadenaDot,"|");

                strcat(cadenaDot,"ID= ");
                sprintf(aux,"%d",bloque.llave);
                strcat(cadenaDot,aux);
                strcat(cadenaDot,"|");

                strcat(cadenaDot,"Padre= ");
                strcat(cadenaDot,bloque.padre);
                strcat(cadenaDot,"|");

                strcat(cadenaDot,"Nombre= ");
                strcat(cadenaDot,bloque.nombre);
                strcat(cadenaDot,"|");

                int b;
                for( b=0;b<6;b++){
                    strcat(cadenaDot,"Hijo");
                    sprintf(aux,"%d",b);
                    strcat(cadenaDot,aux);
                    strcat(cadenaDot," = ");
                    //if(bloque.apuntadores[b]==-1){
                        strcat(cadenaDot,"null");
                    //}else{
                      //  sprintf(aux,"%d",bloque.apuntadores[b]);
                        //strcat(cadenaDot,aux);
                    //}
                    if(b<5){
                      strcat(cadenaDot,"|");
                    }

                }





                //crea los enlaces de inodos a bloques
                strcat(cadenaDot,"}\"];");
                strcat(punteros,"inodo");
                sprintf(aux,"%d",inodo.llave);
                strcat(punteros,aux);
                strcat(punteros,"->");
                strcat(punteros,"bloque");
                sprintf(aux,"%d",inodo.directo[rep2]);
                strcat(punteros,aux);
                strcat(punteros,";");


           }
    }

    //instrucciones para crear la imagen png desde el archivo .dot
    strcat(cadenaDot,punteros);
    strcat(cadenaDot,"}");
    fprintf(reporteD, "%s", cadenaDot);
    fflush(reporteD);
    //fclose(reporteD);


    }




    //instrucciones para crear la imagen png desde el archivo .dot
            char rutaDot2 [1000];
            strcpy(rutaDot2, "dot -Tpng ");
            strcat(rutaDot2, rutaDot);
            strcat(rutaDot2, " -o ");
            strcat(rutaDot2, rutaCarpeta2);
            strcat(rutaDot2, "/RutaInodosBloques.png");
            system(rutaDot2);
            //abre la imagen con el editor de imagenes shotwell
            char rutaPNG [1000];
            strcpy(rutaPNG, "shotwell ");
            strcat(rutaPNG, rutaCarpeta2);
            strcat(rutaPNG, "/RutaInodosBloques.png");
            system(rutaPNG);





    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);


}















//repore c
void BloquesArchivo(){
    time_t tiempo = time(0);
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (strcmp(id, indexDisco.id)==0) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (strcmp(nombre, mbr.particion[z].nombre)==0) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    char rutaD[50];
    printf("Ingrese la direccion donde se encuentra ubicado el archivo o carpeta: ");
    scanf("%s", &rutaD);
    SuperBloque super;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&super,sizeof(SuperBloque),1,archivo);
    Log bitacora;
    int tipoLog; //si es archivo o carpeta, cero=archivo,1=carpeta
    fseek(archivo,super.apunBloque+super.numeroBloques*sizeof(BloqueExt3),SEEK_SET);
    while(fread(&bitacora,sizeof(Log),1,archivo)){
        if(strcmp(rutaD,bitacora.nombre)==0){
            tipoLog=bitacora.tipo;
            break;
        }
    }

    if(tipoLog==1){
        char *token;
        char nomb[20];//nombre de la carpeta
        token=strtok(rutaD,"/");
        while (token != NULL){
            strcpy(nomb,token);

            token = strtok(NULL,"/");
       }
        BloqueExt3 aux;
        fseek(archivo,super.apunBloque,SEEK_SET);
        while(fread(&aux,sizeof(BloqueExt3),1,archivo)){
            if(strcmp(nomb,aux.nombre)==0){
                break;
            }
        }
        char rutaDot[60];//ruta para crear el archivo .dot
        strcpy(rutaDot, rutaCarpeta);
        strcat(rutaDot, "/BloquesArchivo.dot");
        FILE *reporteD = fopen(rutaDot, "w");
        int i;
        char cadenaDot[10000] = "";//cadena donde se guarda las instrucciones de graphiz
        strcat(cadenaDot, "digraph structs { subgraph cluster0 { struct34 [shape=record,label=\"{");
        strcat(cadenaDot,"Llave: ");
        char llave[10];
        sprintf(llave,"%d",aux.llave);
        strcat(cadenaDot,llave);
        strcat(cadenaDot," ,Nombre: ");
        strcat(cadenaDot,aux.nombre);
        strcat(cadenaDot," ,Padre: ");
        strcat(cadenaDot,aux.padre);
        int y;
        for(y=0;y<6;y++){
            strcat(cadenaDot,"\n apuntador");
            char p[10];
            sprintf(p,"%d",y+1);
            strcat(cadenaDot,p);
            strcat(cadenaDot,": ");
            sprintf(p,"%d",aux.apuntadores[y]);
            strcat(cadenaDot,p);
        }

        strcat(cadenaDot,"}");
        strcat(cadenaDot, "\"];}}");
        fprintf(reporteD, "%s", cadenaDot);
        printf("\n");
        fflush(reporteD);
        fclose(reporteD);
        fflush(archivo);
        fclose(archivo);
        fflush(archivoIndex);
        fclose(archivoIndex);
        //instrucciones para crear la imagen png desde el archivo .dot
        char rutaDot2 [1000];
        strcpy(rutaDot2, "dot -Tpng ");
        strcat(rutaDot2, rutaDot);
        strcat(rutaDot2, " -o ");
        strcat(rutaDot2, rutaCarpeta);
        strcat(rutaDot2, "/bloquesArchivo.png");
        system(rutaDot2);
        //abre la imagen con el editor de imagenes shotwell
        char rutaPNG [1000];
        strcpy(rutaPNG, "shotwell ");
        strcat(rutaPNG, rutaCarpeta);
        strcat(rutaPNG, "/bloquesArchivo.png");
        system(rutaPNG);

    }
    else{
        char *token;
        char nomb[20];//nombre de la carpeta
        token=strtok(rutaD,"/");
        while (token != NULL){
            strcpy(nomb,token);

            token = strtok(NULL,"/");
       }
        BloqueExt3 aux;
        fseek(archivo,super.apunBloque,SEEK_SET);
        char rutaDot[60];
        char cadenaDot[10000] = "";//cadena donde se guarda las instrucciones de graphiz
        strcpy(rutaDot, rutaCarpeta);
        strcat(rutaDot, "/BloquesArchivo.dot");
        FILE *reporteD = fopen(rutaDot, "w");
        strcat(cadenaDot, "digraph structs { subgraph cluster0 { struct34 [shape=record,label=\"{");
        while(fread(&aux,sizeof(BloqueExt3),1,archivo)){
            if(strcmp(nomb,aux.nombre)==0){
                //ruta para crear el archivo .dot

                strcat(cadenaDot,"Llave: ");
                char llave[10];
                sprintf(llave,"%d",aux.llave);
                strcat(cadenaDot,llave);
                strcat(cadenaDot," ,Nombre: ");
                strcat(cadenaDot,aux.nombre);
                strcat(cadenaDot," ,Padre: ");
                strcat(cadenaDot,aux.padre);
                int y;
                for(y=0;y<6;y++){
                    strcat(cadenaDot,"\napuntador");
                    char p[10];
                    sprintf(p,"%d",y+1);
                    strcat(cadenaDot,p);
                    strcat(cadenaDot,": ");
                    sprintf(p,"%d",aux.apuntadores[y]);
                    strcat(cadenaDot,p);
                }
                strcat(cadenaDot,"\nContenido: \n");
                strcat(cadenaDot,aux.contenido);
                strcat(cadenaDot,"|");
            }
        }
        strcat(cadenaDot,"}");
        strcat(cadenaDot, "\"];}}");
        fprintf(reporteD, "%s", cadenaDot);
        printf("\n");
        fflush(reporteD);
        fclose(reporteD);
        fflush(archivo);
        fclose(archivo);
        fflush(archivoIndex);
        fclose(archivoIndex);
        //instrucciones para crear la imagen png desde el archivo .dot
        char rutaDot2 [1000];
        strcpy(rutaDot2, "dot -Tpng ");
        strcat(rutaDot2, rutaDot);
        strcat(rutaDot2, " -o ");
        strcat(rutaDot2, rutaCarpeta);
        strcat(rutaDot2, "/bloquesArchivo.png");
        system(rutaDot2);
        //abre la imagen con el editor de imagenes shotwell
        char rutaPNG [1000];
        strcpy(rutaPNG, "shotwell ");
        strcat(rutaPNG, rutaCarpeta);
        strcat(rutaPNG, "/bloquesArchivo.png");
        system(rutaPNG);


    }

}











void BitmapBloque(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (strcmp(id, indexDisco.id)==0) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (strcmp(nombre, mbr.particion[z].nombre)==0) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    char rutaBit[200];
    strcpy(rutaBit,rutaCarpeta);
    strcat(rutaBit,"/reporteBitmapBloques.csv");
    FILE *archivoNuevo = fopen(rutaBit,"w");
    SuperBloque super;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&super,sizeof(SuperBloque),1,archivo);
    char p[1];
    int cont=0;
    char texto[10000];
    strcpy(texto,"");
    int it;
    fseek(archivo,super.apunBitMatFicheros+super.numeroBloques,SEEK_SET);
    for(it=0;it<super.numeroBloques;it++){
        fread(&p,1,1,archivo);
        if(cont<50){
            strcat(texto,p);
            strcat(texto,",");
            cont=cont+1;
        }
        else{
            strcat(texto,p);
            strcat(texto,"\n");
            cont=0;
        }
    }
    fwrite(texto,strlen(texto),1,archivoNuevo);
    fflush(archivo);
    fclose(archivo);
    fflush(archivoNuevo);
    fclose(archivoNuevo);
    char rutaCsv[300];
    strcpy(rutaCsv,"libreoffice ");
    strcat(rutaCsv, rutaBit);
    system(rutaCsv);
}

void BitmapInodos(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (strcmp(id, indexDisco.id)==0) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (strcmp(nombre, mbr.particion[z].nombre)==0) {
            break;
        }
    }
    FILE *archivoNuevo = fopen("/home/jose/Documentos/Discos/reporteBitmapInodos.csv","rb+");
    if(archivoNuevo==0){
        archivoNuevo=fopen("/home/jose/Documentos/Discos/reporteBitmapInodos.csv","w");
    }
    SuperBloque super;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&super,sizeof(SuperBloque),1,archivo);
    char p[1];
    int cont=0;
    char texto[10000];
    strcpy(texto,"");
    int it;
    fseek(archivo,super.apunBitMatFicheros,SEEK_SET);
    for(it=0;it<super.numeroBloques;it++){
        fread(&p,1,1,archivo);
        if(cont<50){
            strcat(texto,p);
            strcat(texto,",");
            cont=cont+1;
        }
        else{
            strcat(texto,p);
            strcat(texto,"\n");
            cont=0;
        }
    }
    fwrite(texto,strlen(texto),1,archivoNuevo);
    fflush(archivo);
    fclose(archivo);
    fflush(archivoNuevo);
    fclose(archivoNuevo);
    char rutaCsv[300];
    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, "/home/jose/Documentos/Discos/reporteBitmapInodos.csv");
    system(rutaCsv);
}
void reporteBitacora(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (strcmp(id, indexDisco.id)==0) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (strcmp(nombre, mbr.particion[z].nombre)==0) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    FILE *archivoNuevo = fopen("/home/jose/Documentos/Discos/reporteBitacora.csv","rb+");
    if(archivoNuevo==0){
        archivoNuevo=fopen("/home/jose/Documentos/Discos/reporteBitacora.csv","w");
    }
    SuperBloque super;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&super,sizeof(SuperBloque),1,archivo);
    Log aux;
    fseek(archivo,super.apunBloque+super.numeroBloques*sizeof(BloqueExt3),SEEK_SET);
    fseek(archivoNuevo,0,SEEK_SET);
    char cadena[1000];
    strcpy(cadena,"Operacion,Tipo,Path,Contenido\n");
    fwrite(&cadena,strlen(cadena),1,archivoNuevo);
    while(fread(&aux,sizeof(Log),1,archivo)){

        if(aux.tipo!=0 && aux.tipo!=1){
            break;
        }
        strcpy(cadena,aux.tipoOperacion);
        strcat(cadena,",");
        char p[10];
        sprintf(p,"%d",aux.tipo);
        strcat(cadena,p);
        strcat(cadena,",");
        strcat(cadena,aux.nombre);
        strcat(cadena,",");
        if(aux.tipo==0&strcmp(aux.tipoOperacion,"Eliminacion archivo")==0){
            strcat(cadena,"\"");
            strcat(cadena,aux.contenido);
            strcat(cadena,"\"");
        printf("\n contenido bitacora\n %s\n",aux.contenido);
        }else{
            strcat(cadena,"-----");
        }
        strcat(cadena,"\n");
        fwrite(&cadena,strlen(cadena),1,archivoNuevo);
    }
    fflush(archivo);
    fclose(archivo);
    fflush(archivoNuevo);
    fclose(archivoNuevo);
    char rutaCsv[300];
    strcpy(rutaCsv, "libreoffice ");
    strcat(rutaCsv, "/home/jose/Documentos/Discos/reporteBitacora.csv");
    system(rutaCsv);

}


void BloquesID(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (strcmp(id, indexDisco.id)==0) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (strcmp(nombre, mbr.particion[z].nombre)==0) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    int idB;
    printf("Ingrese el ID del bloque que desea consultar: ");
    scanf("%d", &idB);
    SuperBloque super;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&super,sizeof(SuperBloque),1,archivo);
    BloqueExt3 aux;
    fseek(archivo,super.apunBloque,SEEK_SET);
    while(fread(&aux,sizeof(BloqueExt3),1,archivo)){
        if(aux.llave==idB){
            break;
        }
    }
    char rutaDot[60];//ruta para crear el archivo .dot
    strcpy(rutaDot, rutaCarpeta);
    strcat(rutaDot, "/BloquesID.dot");
    FILE *reporteD = fopen(rutaDot, "w");
    char cadenaDot[10000] = "";//cadena donde se guarda las instrucciones de graphiz
    strcat(cadenaDot, "digraph structs { subgraph cluster0 { struct34 [shape=record,label=\"{");
    strcat(cadenaDot,"Llave: ");
    char llave[10];
    sprintf(llave,"%d",aux.llave);
    strcat(cadenaDot,llave);
    strcat(cadenaDot," ,Nombre: ");
    strcat(cadenaDot,aux.nombre);
    strcat(cadenaDot," ,Padre: ");
    strcat(cadenaDot,aux.padre);
    int y;
    for(y=0;y<6;y++){
        strcat(cadenaDot,"\n apuntador");
        char p[10];
        sprintf(p,"%d",y+1);
        strcat(cadenaDot,p);
        strcat(cadenaDot,": ");
        sprintf(p,"%d",aux.apuntadores[y]);
        strcat(cadenaDot,p);
    }
    strcat(cadenaDot,"\nContenido: \n");
    strcat(cadenaDot,aux.contenido);

    strcat(cadenaDot,"}");
    strcat(cadenaDot, "\"];}}");
    fprintf(reporteD, "%s", cadenaDot);
    printf("\n");
    fflush(reporteD);
    fclose(reporteD);
    fflush(archivo);
    fclose(archivo);
    fflush(archivoIndex);
    fclose(archivoIndex);
    //instrucciones para crear la imagen png desde el archivo .dot
    char rutaDot2 [1000];
    strcpy(rutaDot2, "dot -Tpng ");
    strcat(rutaDot2, rutaDot);
    strcat(rutaDot2, " -o ");
    strcat(rutaDot2, rutaCarpeta);
    strcat(rutaDot2, "/bloquesID.png");
    system(rutaDot2);
    //abre la imagen con el editor de imagenes shotwell
    char rutaPNG [1000];
    strcpy(rutaPNG, "shotwell ");
    strcat(rutaPNG, rutaCarpeta);
    strcat(rutaPNG, "/bloquesID.png");
    system(rutaPNG);


}




void super(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticionExt3(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----No se pueden crear archivos en una particion extendida-----\n\n");
        return;
    }else if(tipoP==1){
        printf("-----La particion no posee un sistema de archivos Ext3-----\n\n");
        return;
    }




    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }

    if(mbr.particion[z].tipoFormato!=3){
        printf("\n---La particion no posee un sistema de archivos Ext3---\n\n");
        return;
    }

    SuperBloque superBloque;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&superBloque, sizeof (SuperBloque), 1, archivo);

    printf("\nInformacion superbloque\n");
    printf("\nNumero de inodos: %d",superBloque.numeroInodos);
    printf("\nNumero de bloques: %d",superBloque.numeroBloques);
    printf("\nTamanio bloque: %d",superBloque.tamanioBloque);
    printf("\nNumero magico: %d",superBloque.numeroMagico);
    printf("\nInodos libres: %d",superBloque.inodosLibres);
    printf("\nBloques libres: %d",superBloque.bloquesLibres);
    printf("\n\n");





    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);


}























int replace(char *str, char *orig, char *rep)
{
  static char buffer[4096];

  char *p;

  if(!(p = strstr(str, orig)))  // Is 'orig' even in 'str'?
    return -1;

  strncpy(buffer, str, p-str); // Copy characters from 'str' start to 'orig' st$
  buffer[p-str] = '\0';

  sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));


  strcpy(str,buffer);
  return 0;
}


//Fase 3

void crearScript(){
    printf("Id del disco: ");
    char id[32];

    scanf("%s", &id);
    if (existeDiscoID(id) == 0) {
        printf("-----El disco no existe-----\n\n");
        return;
    }

    IndexDisco indexDisco;
    FILE *archivoIndex = fopen(rutaGeneral, "rb+");

    while (fread(&indexDisco, sizeof (IndexDisco), 1, archivoIndex)) {

        if (!strcmp(id, indexDisco.id)) {
            break;
        }
    }
    char nombre[32];
    printf("Nombre de la particion: ");
    scanf("%s", &nombre);
    int tipoP = existeParticion13(nombre, indexDisco.nombre);
    if (tipoP == 0) {
        printf("-----La particion no existe-----\n\n");
        return;
    } else if (tipoP == -1) {
        printf("-----En una particion extendida no existen archivos-----\n\n");
        return;
    }

    MBR mbr;
    char ruta [60];
    strcpy(ruta, rutaCarpeta);
    strcat(ruta, "/");
    strcat(ruta, indexDisco.nombre);
    strcat(ruta, ".vd");

    FILE *archivo = fopen(ruta, "rb+");
    fread(&mbr, sizeof (MBR), 1, archivo);
    int z;
    for (z = 0; z < 16; z++) {
        if (!strcmp(nombre, mbr.particion[z].nombre)) {
            break;
        }
    }


    if(mbr.particion[z].tipoFormato!=2){
        printf("\n---La particion no posee un sistema de archivos enlazado---\n\n");
        return;
    }

    printf("Ingrese el nombre del archivo csv");
    char nombreArchivo[32];
    scanf("%s",&nombreArchivo);
    ArchivoEnlazado archivoEnlazado;
    fseek(archivo,mbr.particion[z].inicio,SEEK_SET);
    fread(&archivoEnlazado, sizeof (archivoEnlazado), 1, archivo);
    if(strcmp(nombreArchivo,archivoEnlazado.nombre)){
        printf("No existe el archivo\n\n");
        return;
    }

    char rutaCSV[60];
    strcpy(rutaCSV,rutaCarpeta);
    strcat(rutaCSV,"/temporalCSV.csv");

    FILE *archivoCSV = fopen(rutaCSV, "wb+");
    char contenido[100];
    int i;
    fseek(archivo,mbr.particion[z].inicio+sizeof(ArchivoEnlazado),SEEK_SET);
    for(i=archivoEnlazado.inicio;i<archivoEnlazado.fin;i++){
        fseek(archivo,4,SEEK_CUR);
        fread(&contenido, 100, 1, archivo);
        fwrite(&contenido, 100, 1, archivoCSV);

    }



    char rutaScript[200];
    strcpy(rutaScript,rutaCarpeta);
    strcat(rutaScript,"/scripts/");
    strcat(rutaScript,archivoEnlazado.nombre);
    strcat(rutaScript,".sql");
    FILE *script = fopen(rutaScript, "wb+");

    fseek(archivoCSV,0,SEEK_SET);
    char contenidoLinea[10000];


    fgets(contenidoLinea, 10000, archivoCSV);

    char encabezadoConsulta[10000];
    strcpy(encabezadoConsulta,"INSERT INTO ");
    int cantidadConsultas=0;
    strcat(encabezadoConsulta,archivoEnlazado.nombre);
    strcat(encabezadoConsulta,"temp");
    strcat(encabezadoConsulta,"(");
    int indiceEncabezado=0;
    int esVenta=strcmp(archivoEnlazado.nombre,"ventas");
    int esCompra=strcmp(archivoEnlazado.nombre,"compra");
    int esVoucher=strcmp(archivoEnlazado.nombre,"voucher");
    char *token;
    token = strtok(contenidoLinea, ",");
    while (token != NULL){
        if(esVenta==0){
            if(indiceEncabezado<=24){
                strcat(encabezadoConsulta,token);
            }
            if(indiceEncabezado==24){
                break;
            }
        }else if(esCompra==0){


            switch (indiceEncabezado) {
            case 0:
                strcat(encabezadoConsulta,"IDC");
                break;
            case 15:
                strcat(encabezadoConsulta,"DOS_G");
                break;
            case 16:
                strcat(encabezadoConsulta,"TRES_G");
                break;
            case 17:
                strcat(encabezadoConsulta,"CUATRO_G");
                break;
            case 20:
                strcat(encabezadoConsulta,"WI_FI");
                break;
            default:
                strcat(encabezadoConsulta,token);
                break;
            }


        }else if(esVoucher==0 && indiceEncabezado==0){

            strcat(encabezadoConsulta,"NO_VOUCHER");
        }

        else{

            strcat(encabezadoConsulta,token);

        }
            //printf("El token es: %s\n",token);

            token = strtok(NULL,",");
            if(token!=NULL){

                strcat(encabezadoConsulta,",");

            }
            indiceEncabezado++;
    }


    if(strcmp(archivoEnlazado.nombre,"asignacion")==0){



        while (fgets(contenidoLinea, 10000, archivoCSV) != NULL)
        {
           int estado=0;
           int indice=0;
           char consulta[10000];
           strcpy(consulta,encabezadoConsulta);
           strcat(consulta,") VALUES(");

           if(cantidadConsultas==20000){
            printf("sdfsad");
           }

           replace(contenidoLinea,",,",",----,");
           token=strtok(contenidoLinea,",");
           int j;

           while (token != NULL){
                   estado=1;

                       if(strcmp(token,"----")==0){
                            strcpy(token,"null");
                       }



                   if(indice==0 || indice==1 || indice==13 || indice==14 || indice==15){
                       strcat(consulta,token);
                   }else if(indice==16){

                       char cadenaFecha[30];
                       strcpy(cadenaFecha,token);

                       char *dia=strtok(cadenaFecha,"/");
                       int tamdia=strlen(dia);
                       if(tamdia==1){
                           strcpy(cadenaFecha,"0");
                           strcat(cadenaFecha,token);

                       }else{
                           strcpy(cadenaFecha,token);
                       }
                       dia=strtok(NULL,"@");




                       strcat(consulta,"TO_DATE(");
                       strcat(consulta,"'");
                       strcat(consulta,cadenaFecha);
                       strcat(consulta,"',' dd/mm/yyyy hh24:mi:ss')");
                       strcpy(cadenaFecha,"");
                       strcpy(token,"");

                   }else{
                       strcat(consulta,"'");
                       strcat(consulta,token);
                       strcat(consulta,"'");
                   }

                   //printf("El token es: %s\n",token);

                   token = strtok(NULL,",");
                   if(token!=NULL){
                       strcat(consulta,",");
                   }
                   indice++;


           }


           if(estado==1){
           strcat(consulta,");\n");
           fwrite(&consulta,strlen(consulta), 1,script);
           cantidadConsultas++;
            }
        }




    }else if(strcmp(archivoEnlazado.nombre,"compra")==0){



        while (fgets(contenidoLinea, 10000, archivoCSV) != NULL)
        {
           int estado=0;
           int indice=0;
           char consulta[10000];

           strcpy(consulta,encabezadoConsulta);
           strcat(consulta,") VALUES(");

           replace(contenidoLinea,",,",",----,");
           token=strtok(contenidoLinea,",");
           while (token != NULL){
                   estado=1;

                   if(strcmp(token,"----")==0){
                        strcpy(token,"null");
                   }

                   if(indice==0 || indice==8 || indice==11 || (indice>=14&&indice<=31)||indice==35){

                       //strcat(consulta,token);
                       strcat(consulta,token);
                   }else if(indice==32){

                       char cadenaFecha[30];
                       int it;
                       for(it=0;it<2;it++){
                            char c=token[it];
                            if(c=='/'){
                                break;
                            }
                       }

                       if(it==1){
                           strcpy(cadenaFecha,"0");
                           strcat(cadenaFecha,token);

                       }else{
                           strcpy(cadenaFecha,token);
                       }


                       strcat(consulta,"TO_DATE(");
                       strcat(consulta,"'");
                       strcat(consulta,cadenaFecha);
                       strcat(consulta,"',' dd/mm/yyyy hh24:mi:ss')");


                   }else{
                       strcat(consulta,"'");
                       strcat(consulta,token);
                       strcat(consulta,"'");
                   }

                   //printf("El token es: %s\n",token);
                   token = strtok(NULL,",");
                   if(token!=NULL){
                       strcat(consulta,",");
                   }
                   indice++;

           }
           if(estado==1){
           strcat(consulta,");\n");
           fwrite(&consulta,strlen(consulta), 1,script);
           cantidadConsultas++;
            }
        }



    }else if(strcmp(archivoEnlazado.nombre,"kiosco")==0){



        while (fgets(contenidoLinea, 10000, archivoCSV) != NULL)
        {
           int estado=0;
           int indice=0;
           char consulta[10000];
           strcpy(consulta,encabezadoConsulta);
           strcat(consulta,") VALUES(");


           replace(contenidoLinea,",,",",----,");
           token=strtok(contenidoLinea,",");
           while (token != NULL){
                   estado=1;

                   if(strcmp(token,"----")==0){
                        strcpy(token,"null");
                   }

                   if(indice==0){
                       strcat(consulta,token);
                   }else if(indice==16){


                       char cadenaFecha[30];
                       strcpy(cadenaFecha,token);

                       char *dia=strtok(cadenaFecha,"/");
                       int tamdia=strlen(dia);
                       if(tamdia==1){
                           strcpy(cadenaFecha,"0");
                           strcat(cadenaFecha,token);

                       }else{
                           strcpy(cadenaFecha,token);
                       }
                       dia=strtok(NULL,"@");

                       strcat(consulta,"TO_DATE(");
                       strcat(consulta,"'");
                       strcat(consulta,cadenaFecha);
                       strcat(consulta,"',' dd/mm/yyyy hh24:mi:ss')");

                   }else{
                       strcat(consulta,"'");
                       strcat(consulta,token);
                       strcat(consulta,"'");
                   }

                   //printf("El token es: %s\n",token);
                   token = strtok(NULL,",");
                   if(token!=NULL){
                       strcat(consulta,",");
                   }
                   indice++;

           }
           if(estado==1){
           strcat(consulta,");\n");
           fwrite(&consulta,strlen(consulta), 1,script);
           cantidadConsultas++;
            }
        }




    }else if(strcmp(archivoEnlazado.nombre,"pagos")==0){



        while (fgets(contenidoLinea, 10000, archivoCSV) != NULL)
        {
           int estado=0;
           int indice=0;
           char consulta[10000];
           strcpy(consulta,encabezadoConsulta);
           strcat(consulta,") VALUES(");

           replace(contenidoLinea,",,",",----,");
           token=strtok(contenidoLinea,",");
           while (token != NULL){
                   estado=1;

                   if(strcmp(token,"----")==0){
                        strcpy(token,"null");
                   }

                   if(indice==0 || indice==1 || indice==8){
                       strcat(consulta,token);
                   }else if(indice==7){


                       char cadenaFecha[30];
                       int it;
                       for(it=0;it<2;it++){
                            char c=token[it];
                            if(c=='/'){
                                break;
                            }
                       }

                       if(it==1){
                           strcpy(cadenaFecha,"0");
                           strcat(cadenaFecha,token);

                       }else{
                           strcpy(cadenaFecha,token);
                       }


                       strcat(consulta,"TO_DATE(");
                       strcat(consulta,"'");
                       strcat(consulta,cadenaFecha);
                       strcat(consulta,"',' dd/mm/yyyy hh24:mi:ss')");

                   }else{
                       strcat(consulta,"'");
                       strcat(consulta,token);
                       strcat(consulta,"'");
                   }

                   //printf("El token es: %s\n",token);
                   token = strtok(NULL,",");
                   if(token!=NULL){
                       strcat(consulta,",");
                   }
                   indice++;

           }
           if(estado==1){
           strcat(consulta,");\n");
           fwrite(&consulta,strlen(consulta), 1,script);
           cantidadConsultas++;
            }
        }



    }else if(strcmp(archivoEnlazado.nombre,"ventas")==0){



        while (fgets(contenidoLinea, 10000, archivoCSV) != NULL)
        {
           int estado=0;
           int indice=0;
           char consulta[10000];
           strcpy(consulta,encabezadoConsulta);
           strcat(consulta,") VALUES(");

           replace(contenidoLinea,",,",",----,");


           token=strtok(contenidoLinea,",");
           while (token != NULL){
                   estado=1;


                   if(strcmp(token,"----")==0){
                        strcpy(token,"null");
                   }

                   if(indice>=24 && strcmp(token,"null")==0){
                   break;
                   }

                   if(indice==0 || indice==1 || indice==9 || indice==21 || indice==22 || indice==24){
                       if(indice==21){
                           char cad[10];
                           strcpy(cad,token);
                           replace(cad," ","");
                           strcat(consulta,cad);
                       }else{
                       strcat(consulta,token);
                       }
                   }else if(indice==7 || indice==23){


                       char cadenaFecha[30];
                       int it;
                       for(it=0;it<2;it++){
                            char c=token[it];
                            if(c=='/'){
                                break;
                            }
                       }

                       if(it==1){
                           strcpy(cadenaFecha,"0");
                           strcat(cadenaFecha,token);

                       }else{
                           strcpy(cadenaFecha,token);
                       }


                       strcat(consulta,"TO_DATE(");
                       strcat(consulta,"'");
                       strcat(consulta,cadenaFecha);
                       strcat(consulta,"',' dd/mm/yyyy hh24:mi:ss')");

                   }else{
                       strcat(consulta,"'");
                       strcat(consulta,token);
                       strcat(consulta,"'");
                   }

                   //printf("El token es: %s\n",token);
                   token = strtok(NULL,",");
                   if(token!=NULL &&indice<24){
                       strcat(consulta,",");
                   }
                   indice++;

           }
           if(estado==1){
           strcat(consulta,");\n");
           fwrite(&consulta,strlen(consulta), 1,script);
           cantidadConsultas++;
            }
        }




    }else if(strcmp(archivoEnlazado.nombre,"voucher")==0){



        while (fgets(contenidoLinea, 10000, archivoCSV) != NULL)
        {
           int estado=0;
           int indice=0;
           char consulta[10000];
           strcpy(consulta,encabezadoConsulta);
           strcat(consulta,") VALUES(");


           replace(contenidoLinea,",,",",----,");
           token=strtok(contenidoLinea,",");
           while (token != NULL){
                   estado=1;


                   if(strcmp(token,"----")==0){
                        strcpy(token,"null");
                   }



                   if(indice==0 || indice==6){
                       strcat(consulta,token);
                   }else if(indice==5){


                       char cadenaFecha[30];
                       int it;
                       for(it=0;it<2;it++){
                            char c=token[it];
                            if(c=='/'){
                                break;
                            }
                       }

                       if(it==1){
                           strcpy(cadenaFecha,"0");
                           strcat(cadenaFecha,token);

                       }else{
                           strcpy(cadenaFecha,token);
                       }


                       strcat(consulta,"TO_DATE(");
                       strcat(consulta,"'");
                       strcat(consulta,cadenaFecha);
                       strcat(consulta,"',' yyyy/mm/dd hh24:mi:ss')");

                   }else{
                       strcat(consulta,"'");
                       strcat(consulta,token);
                       strcat(consulta,"'");
                   }

                   //printf("El token es: %s\n",token);
                   token = strtok(NULL,",");
                   if(token!=NULL){
                       strcat(consulta,",");
                   }
                   indice++;

           }
           if(estado==1){
           strcat(consulta,");\n");
           fwrite(&consulta,strlen(consulta), 1,script);
           cantidadConsultas++;
            }
        }





    }else{

    }

    fflush(script);
    fclose(script);

    fflush(archivoIndex);
    fclose(archivoIndex);
    fflush(archivo);
    fclose(archivo);
    fflush(archivoCSV);
    fclose(archivoCSV);

    printf("\n*****************************************************\n");
    printf("Se han generado %d INSERT",cantidadConsultas);
    printf("\n*****************************************************\n");

    char rutaCsv[1000];
    strcpy(rutaCsv, "gedit ");
    strcat(rutaCsv, rutaScript);
    system(rutaCsv);
    remove(rutaCSV);
}


//fin
















