from django.http import HttpResponse,Http404
from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import render_to_response
from Fase3MIA.forms import loginForm
from Fase3MIA.Empleado import Empleado


from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect

import cx_Oracle

def obtenerConexion():
	conexion=cx_Oracle.connect('Fase3MIA/201213402@127.0.0.1/XE')
	return conexion

def conexion():
	conexion=cx_Oracle.connect('Fase3MIA/201213402@127.0.0.1/XE')
	cursor=conexion.cursor()
	return cursor


def login_viewTemp(request):

	mensaje=""

	if request.method=="POST":
		form=loginForm(request.POST)
		if form.is_valid():	
			username=form.cleaned_data['username']
			password=form.cleaned_data['password']
			return render(request,'inicio.html',{'activo':not None,'username':username,'password':password})

		else:
			return HttpResponse("No valido")
		
	if activo:
		return HttpResponseRedirect('/inicio/')
	else:
		form=loginForm() 
		ctx={'form':form,'mensaje':mensaje}
		return render_to_response('login.html',ctx,context_instance=RequestContext(request))


def login_view(request):
	try:
		del request.session["usuario"]
		del request.session["password"]
		del request.session["activo"]
		del request.session["superUsuario"]
		del request.session["administrador"]
		del request.session["vendedor"]

	except KeyError:
		pass
	return render(request,'login.html')

def logout_view(request):
	try:
		del request.session["usuario"]
		del request.session["password"]
		del request.session["activo"]
		del request.session["superUsuario"]
		del request.session["administrador"]
		del request.session["vendedor"]

	except KeyError:
		pass
	return HttpResponseRedirect('/inicio/')
	#return render(request,'login.html')

def inicio(request):
	usuario=None
	activo=None
	superUsuario=""
	aministrador=""
	vendedor=""
	try:
		user=request.session["usuario"]
		usuario=user[0]
		activo=request.session["activo"]
		if user[3]==0:
			superUsuario="Super Usuario"
		if user[3]==1:
			aministrador="Administrador"
		if user[3]==2:
			vendedor="Vendedor"
	except KeyError:
		pass

	return render(request,'inicio.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor})

def login(request):
	usuario=None
	activo=None
	superUsuario=""
	aministrador=""
	vendedor=""

	try:
		user=request.session["usuario"]
		usuario=user[0]
		activo=request.session["activo"]
		
		return HttpResponseRedirect('/inicio/')
	except KeyError:
		pass		
	erroresLogin=[]
	usuario=""
	password=""
	activo=None
	if request.method=="POST":
		usuario=request.POST.get('usuario')
		password=request.POST.get('password')
		if not request.POST.get('usuario',''):
			erroresLogin.append('Por favor introduzca su email')
		if not request.POST.get('password',''):
			erroresLogin.append('Por favor introducza su contrasena')

		if not erroresLogin:			
			cursor=conexion()
			cursor.execute('select nombre,correo,contrasena,tipo from Empleado where contrasena=\''+password+'\''+'and correo=\''+usuario+'\'' )
			user=cursor.fetchall()
			if user:
				request.session["usuario"]=user[0]
				request.session["activo"]=not None
				usuario=user[0][0]
				activo=not None
				superUsuario=""
				aministrador=""
				vendedor=""

				return HttpResponseRedirect('/inicio/')
			else:
				usuario=request.POST.get('usuario')
				erroresLogin.append('Usuario invalido')
				return render(request,'login.html',{'erroresLogin':erroresLogin,'usuario':usuario})
	return render(request,'login.html',{'erroresLogin':erroresLogin,'usuario':usuario,'password':password,'activo':activo})
	
def crearAdministrador(request):
	usuario=None
	activo=None
	superUsuario=""
	aministrador=""
	vendedor=""
	erroresCrear=[]
	try:
		user=request.session["usuario"]
		usuario=user[0]
		activo=request.session["activo"]

		if user[3]==0:
			superUsuario="Super Usuario"
		if user[3]==1:
			aministrador="Administrador"
		if user[3]==2:
			vendedor="Vendedor"

	except KeyError:
		pass	

	if request.method=="POST":
		dpi=request.POST.get('dpi')
		nombre=request.POST.get('nombre')
		telefono=request.POST.get('telefono')
		correo=request.POST.get('correo')
		contrasena=request.POST.get('password')
		direccion=request.POST.get('direccion')
		tipo=request.POST.get('tipo')
		cursor=conexion()
		cursor.execute('select correo,contrasena,dpi from empleado where correo=\''+correo+'\' or contrasena=\''+contrasena+'\' or dpi=\''+dpi+'\'')
		user=cursor.fetchall()
		empleado=None
		if user:
			empleado=user[0]

		if empleado:
			contrasena=""
			
			if empleado[0]==correo:
				erroresCrear.append('Ya existe un empleado con el mismo correo')
			if empleado[1]==contrasena:
				erroresCrear.append('Ya existe un empleado con la misma contrasena')
			if empleado[2]==dpi:
				erroresCrear.append('Ya existe un empleado con el mismo dpi')

			return render(request,'registrarEmpleado.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor,'dpi':dpi,'nombre':nombre,'telefono':telefono,'correo':correo,'contrasena':contrasena,'direccion':direccion,'erroresCrear':erroresCrear})
		else:
			#system.getDate
			fechaContrato='TO_DATE(\'18/11/2014\',\'dd/mm/yyyy\')'
			conexion2=obtenerConexion()
			cursor=conexion2.cursor()
			#cursor=conexion()
			cursor.execute('insert into empleado(DPI,NOMBRE,CORREO,TELEFONO,CONTRASENA,DIRECCION,TIPO,FECHA_CONTRATO) values(\''+dpi+'\',\''+nombre+'\',\''+correo+'\',\''+telefono+'\',\''+contrasena+'\',\''+direccion+'\','+tipo+','+fechaContrato+')')
			conexion2.commit()
			creado='si'
			return render(request,'registrarEmpleado.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor,'creado':creado})
	else:
		
		return render(request,'registrarEmpleado.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor})



def moduloEliminarEmpleado(request):
	usuario=None
	activo=None
	superUsuario=""
	aministrador=""
	vendedor=""
	erroresCrear=[]
	try:
		user=request.session["usuario"]
		usuario=user[0]
		activo=request.session["activo"]

		if user[3]==0:
			superUsuario="Super Usuario"
		if user[3]==1:
			aministrador="Administrador"
		if user[3]==2:
			vendedor="Vendedor"

	except KeyError:
		pass	

	if request.method=="POST":

		
		
		return HttpResponseRedirect('/inicio/')
	else:
		cursor=conexion()
		if superUsuario:
			cursor.execute('select dpi,nombre,telefono,correo from empleado where tipo=1 or tipo=2')
		if aministrador:
			cursor.execute('select dpi,nombre,telefono,correo from empleado where tipo=2')
		empleados=cursor.fetchall()
		arrayEmpleados=[]
		j=0;
		while (j<len(empleados)):
			emp1=empleados[j]
			emp=Empleado();
			emp.dpi=emp1[0]
			emp.nombre=emp1[1]
			emp.telefono=emp1[2]
			emp.correo=emp1[3]
			arrayEmpleados.append(emp)
			j=j+1
		eliminar='eliminar'
		return render(request,'eliminarEmpleado.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor,'empleados':arrayEmpleados,'eliminar':eliminar})

def elimirarEmpleado(request,dpi):
	usuario=None
	activo=None
	superUsuario=""
	aministrador=""
	vendedor=""
	erroresCrear=[]
	try:
		user=request.session["usuario"]
		usuario=user[0]
		activo=request.session["activo"]

		if user[3]==0:
			superUsuario="Super Usuario"
		if user[3]==1:
			aministrador="Administrador"
		if user[3]==2:
			vendedor="Vendedor"

	except KeyError:
		pass	

	if request.method=="GET":
		conexion2=obtenerConexion()
		cursor=conexion2.cursor()
		cursor.execute('delete from empleado where dpi=\''+dpi+'\'')
		conexion2.commit()

		if superUsuario:
			cursor.execute('select dpi,nombre,telefono,correo from empleado where tipo=1 or tipo=2')
		if aministrador:
			cursor.execute('select dpi,nombre,telefono,correo from empleado where tipo=2')
		empleados=cursor.fetchall()
		arrayEmpleados=[]
		j=0;
		while (j<len(empleados)):
			emp1=empleados[j]
			emp=Empleado();
			emp.dpi=emp1[0]
			emp.nombre=emp1[1]
			emp.telefono=emp1[2]
			emp.correo=emp1[3]
			arrayEmpleados.append(emp)
			j=j+1
		mensaje='Empleado eliminado...'
		eliminar='eliminar'
	return render(request,'eliminarEmpleado.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor,'empleados':arrayEmpleados,'mensaje':mensaje,'eliminar':eliminar})

def moduloModificarEmpleado(request):
	usuario=None
	activo=None
	superUsuario=""
	aministrador=""
	vendedor=""
	erroresCrear=[]
	try:
		user=request.session["usuario"]
		usuario=user[0]
		activo=request.session["activo"]

		if user[3]==0:
			superUsuario="Super Usuario"
		if user[3]==1:
			aministrador="Administrador"
		if user[3]==2:
			vendedor="Vendedor"

	except KeyError:
		pass	

	if request.method=="POST":

		
		
		return HttpResponseRedirect('/inicio/')
	else:
		cursor=conexion()
		if superUsuario:
			cursor.execute('select dpi,nombre,telefono,correo from empleado where tipo=1 or tipo=2')
		if aministrador:
			cursor.execute('select dpi,nombre,telefono,correo from empleado where tipo=2')
		empleados=cursor.fetchall()
		arrayEmpleados=[]
		j=0;
		while (j<len(empleados)):
			emp1=empleados[j]
			emp=Empleado();
			emp.dpi=emp1[0]
			emp.nombre=emp1[1]
			emp.telefono=emp1[2]
			emp.correo=emp1[3]
			arrayEmpleados.append(emp)
			j=j+1
		modificar='modificar'
		return render(request,'eliminarEmpleado.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor,'empleados':arrayEmpleados,'modificar':modificar})

def modificarEmpleado(request,dpi):
	usuario=None
	activo=None
	superUsuario=""
	aministrador=""
	vendedor=""
	erroresCrear=[]
	try:
		user=request.session["usuario"]
		usuario=user[0]
		activo=request.session["activo"]

		if user[3]==0:
			superUsuario="Super Usuario"
		if user[3]==1:
			aministrador="Administrador"
		if user[3]==2:
			vendedor="Vendedor"

	except KeyError:
		pass	

	if request.method=="GET":
		conexion2=obtenerConexion()
		cursor=conexion2.cursor()
		cursor.execute('select * from empleado where dpi=\''+dpi+'\'')

		empleados=cursor.fetchall()

		emp1=empleados[0]
		emp=Empleado();
		emp.dpi=emp1[0]
		emp.nombre=emp1[1]
		emp.correo=emp1[2]
		emp.telefono=emp1[3]
		emp.contrasena=emp1[4]
		emp.direccion=emp1[5]
		emp.fechaContrato=emp1[9]
		emp.tipo=emp1[10]
		#mensaje='Empleado Modificado...'
		modificar='modificar'
	return render(request,'registrarEmpleado.html',{'usuario':usuario,'activo':activo,'superUsuario':superUsuario,'aministrador':aministrador,'vendedor':vendedor,'empleado':emp,'modificar':modificar})
