"""Fase3MIA URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from Fase3MIA import views
import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$',views.login),
    url(r'^login_view/$',views.login_view),
    url(r'^logout/$',views.logout_view),
    url(r'^inicio/$',views.inicio),
    url(r'^crearAdministrador/$',views.crearAdministrador),
    url(r'^eliminarEmpleado/$',views.moduloEliminarEmpleado),
    url(r'^eliminarEmpleado/(?P<dpi>.*)/$',views.elimirarEmpleado),
    url(r'^modificarEmpleado/$',views.moduloModificarEmpleado),
    url(r'^modificarEmpleado/(?P<dpi>.*)/$',views.modificarEmpleado),
    url(r'^media/(?P<path>.*)$','django.views.static.serve',{'document_root':settings.MEDIA_ROOT})
]
